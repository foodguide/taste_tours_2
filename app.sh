#!/usr/bin/env bash

function showDevBanner() {
  cat <<"EOF"
o      O                                                 o
O      o                                                O  o
o      O                                                o
OoOooOOo                                                o
o      O .oOoO' .oOo. .oOo. O   o       .oOo  .oOo. .oOoO  O  'OoOo. .oOoO
O      o O   o  O   o O   o o   O       O     O   o o   O  o   o   O o   O
o      o o   O  o   O o   O O   o       o     o   O O   o  O   O   o O   o
o      O `OoO'o oOoO' oOoO' `OoOO       `OoO' `OoO' `OoO'o o'  o   O `OoOo
                O     O         o                                        O
                o'    o'     OoO'                                     OoO'



                     .oOo                             o oO
                     O           o                   O  OO
                     o                               o  oO
                     OoO                             o  Oo
`oOOoOO. O   o       o    `OoOo. O  .oOo. 'OoOo. .oOoO  oO
 O  o  o o   O       O     o     o  OooO'  o   O o   O
 o  O  O O   o       o     O     O  O      O   o O   o  Oo
 O  o  o `OoOO       O'    o     o' `OoO'  o   O `OoO'o oO
             o
          OoO'
EOF
}

function echoHelp() {
  echo "------------------------------------------------"
  echo "Following commands are supported by this script:"
  echo "------------------------------------------------"
  echo "1."
  echo "./app.sh run (The environment is determined by the .env file"
  echo ""
  echo "2."
  echo "./app.sh run rebuild (force the rebuild of the docker containers)"
  echo ""
  echo "3."
  echo "./app.sh deploy (deploy to aws)"
  echo ""
  echo "4."
  echo "./app.sh update (update the running aws containers to the latest docker container in the aws ecr)"
  echo ""
  echo "5."
  echo "./app.sh restart nuxt (Restarts the frontend container while the other ones keep running)"
  echo ""
  echo "6."
  echo "./app.sh help (Shows this help)"
}

function askForYes() {
  read answr
  if [ "$answr" == "yes" ]; then
    echo "Ok."
  else
    echo "aborting..."
    exit
  fi
}

function checkMaster() {
  BRANCH=$(git symbolic-ref --short -q HEAD)
  if [ $BRANCH != "master" ]; then
    echo "Building from ${BRANCH} is not allowed. Checkout the master branch to build."
    exit
  fi
}

function checkGitClean() {
  if [ -z "$(git status --porcelain)" ]; then
    echo "git status is clear. continue..."
  else
    echo "git status is not clear. aborting..."
    exit
  fi
}

function showLatestGitTag() {
  echo "Latest git tag is:"
  git describe --abbrev=0 --tags
}

function getTag() {
  read tagTextInput
  if [ -z "$tagTextInput" ]; then
    echo "no tag entered. aborting..."
    exit
  fi
  echo "$tagTextInput"
}

function tagGit() {
  git commit -a -m "built for deployment. $1"
  git tag "$1" -a -m "$1"
  git push origin "$1"
  git push
}

function run() {
  echo "This script will do the following:
    1. Start the docker containers which are needed for the development environment
    ----------------------------------
    do you wish to continue?(yes/no)"
  askForYes
  echo "starting dev containers..."
  echo "environment is determined by .env file"
  showDevBanner
  if [[ $1 == "rebuild" ]]; then
    echo "rebuilding"
    docker-compose up --force-recreate --build
  else
    echo "not rebuilding"
    docker-compose up
  fi
}

function buildProd() {
  eval $(aws ecr get-login --no-include-email)
  if [[ $2 == "nuxt" ]]; then
    echo "deploying frontend"
    buildFrontend "$1"
  elif [[ $2 == "strapi" ]]; then
    echo "deploying strapi"
    buildBackend "$1"
  else
    echo "deploying both"
    buildFrontend "$1"
    buildBackend "$1"
  fi
}

function buildFrontend() {
  docker build -t taste-tours-2:"$1" -t taste-tours-2:latest ./webapp
  docker tag taste-tours-2:"$1" 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-2:"$1"
  docker tag taste-tours-2:latest 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-2:latest
  sleep 1
  ecs-cli push "439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-2:$1"
  sleep 1
  ecs-cli push 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-2:latest
  sleep 1
}

function buildBackend() {
  docker build -t taste-tours-strapi:"$1" -t taste-tours-strapi:latest ./strapi
  docker tag taste-tours-strapi:"$1" 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-strapi:"$1"
  docker tag taste-tours-strapi:latest 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-strapi:latest
  sleep 1
  ecs-cli push "439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-strapi:$1"
  sleep 1
  ecs-cli push 439791643524.dkr.ecr.eu-central-1.amazonaws.com/taste-tours-strapi:latest
}

function deploy() {
  echo "deployment script is not yet ready"
  echo "This script will do the following:
    1. Tag the master branch
    2. Commit and push all uncommited changes and the tag to the bitbucket repository
    3. Run the tests and checks if every test passes
    4. Build the docker images
    5. Tag the images
    6. Upload the images to the registry on aws

    ----------------------------------
    do you wish to continue?(yes/no)"
  askForYes
  checkMaster
  checkGitClean
  showLatestGitTag
  echo "Please insert the tag for this new build."
  tag=$(getTag)
  tagGit "$tag"
  buildProd "$tag" "$1"
}

if [[ $1 == "run" ]]; then
  run "$2"
fi

if [[ $1 == "deploy" ]]; then
  deploy "$2"
fi

if [[ $1 == "help" ]]; then
  echoHelp
fi

if [[ $1 == "restart" ]]; then
  if [[ $2 == "nuxt" ]]; then
    echo "restarting frontend"
    docker-compose restart nuxt
  elif [[ $2 == "strapi" ]]; then
    echo "restarting strapi"
    docker-compose restart strapi
  elif [[ $2 == "db" ]]; then
    echo "restarting db"
    docker-compose restart db
  fi
fi
