#!/usr/bin/env bash
set -ea

startProd() {
  echo "starting nuxt in production environment"
  npm run build
  npm run start &

  nuxtPID=$!
  wait "$nuxtPID"
}

startDev() {
  echo "starting nuxt in development environment"
  npm run dev &

  nuxtPID=$!
  wait "$nuxtPID"
}

_stopNuxt() {
  echo "Stopping nuxt"
  kill -SIGINT "$nuxtPID"
  wait "$nuxtPID"
}

trap _stopnuxt SIGTERM SIGINT

cd /usr/src/nuxt

APP_NAME=webapp
API_BASE_URL_BROWSER=${API_BASE_URL_BROWSER:-http://localhost:1337}
API_BASE_URL=${API_BASE_URL:-http://strapi:1337}
echo "starting nuxt"
cd "$APP_NAME"

if [ -d "node_modules/.staging" ]; then
  chown node:node -R node_modules/.staging
fi

npm rebuild node-sass

if [[ $1 == "production" ]]; then
  startProd
fi
if [[ $1 == "prod" ]]; then
  startProd
fi
if [[ $1 == "development" ]]; then
  startDev
fi
if [[ $1 == "dev" ]]; then
  startDev
fi
