import { mount } from '@vue/test-utils'
import LocationCard from '../components/LocationCard'

describe('LocationCard', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(LocationCard)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
