import { mount } from '@vue/test-utils'
import ProductCitySection from '@/components/ProductCitySection.vue'

describe('ProductCitySection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ProductCitySection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
