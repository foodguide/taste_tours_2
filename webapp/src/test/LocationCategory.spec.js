import { mount } from '@vue/test-utils'
import LocationCategory from '../components/LocationCategory'

describe('RestaurantCategories', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(LocationCategory)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
