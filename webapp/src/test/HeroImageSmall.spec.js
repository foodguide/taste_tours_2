import { mount } from '@vue/test-utils'
import HeroImageSmall from '@/components/HeroImageSmall.vue'

describe('HeroImageSmall', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(HeroImageSmall)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
