import { mount } from '@vue/test-utils'
import CallToActionContact from '@/components/CallToActionContact.vue'

describe('CallToActionContact', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CallToActionContact)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
