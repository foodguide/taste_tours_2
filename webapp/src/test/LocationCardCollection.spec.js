import { mount } from '@vue/test-utils'
import LocationCardCollection from '../components/LocationCardCollection'

describe('LocationCardCollection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(LocationCardCollection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
