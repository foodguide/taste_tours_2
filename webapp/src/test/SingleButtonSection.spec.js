import { mount } from '@vue/test-utils'
import SingleButtonSection from '@/components/SingleButtonSection.vue'

describe('SingleButtonSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(SingleButtonSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
