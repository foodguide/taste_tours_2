import { mount } from '@vue/test-utils'
import SocialIcons from '@/components/SocialIcons.vue'

describe('SocialIcons', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(SocialIcons)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
