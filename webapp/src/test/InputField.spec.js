import { mount } from '@vue/test-utils'
import InputField from '@/components/InputField.vue'

describe('InputField', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(InputField)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
