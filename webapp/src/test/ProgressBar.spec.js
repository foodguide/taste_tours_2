import { mount } from '@vue/test-utils'
import ProgressBar from '@/components/ProgressBar.vue'

describe('ProgressBar', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ProgressBar)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
