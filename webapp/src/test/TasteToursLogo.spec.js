import {mount} from '@vue/test-utils'
import TasteToursLogo from '@/components/TasteToursLogo.vue'

describe('TasteToursLogo', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TasteToursLogo)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})