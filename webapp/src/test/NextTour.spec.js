import { mount } from '@vue/test-utils'
import NextTour from '@/components/NextTour.vue'

describe('NextTour', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(NextTour)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
