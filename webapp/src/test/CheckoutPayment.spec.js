import { mount } from '@vue/test-utils'
import CheckoutPayment from '@/components/CheckoutPayment.vue'

describe('CheckoutPayment', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CheckoutPayment)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
