import { mount } from '@vue/test-utils'
import SingleIconText from '@/components/SingleIconText.vue'

describe('SingleIconText', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(SingleIconText)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
