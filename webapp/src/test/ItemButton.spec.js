import { mount } from '@vue/test-utils'
import TwoColorButton from '@/components/TwoColorButton.vue'

describe('TwoColorButton', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TwoColorButton)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
