import { mount } from '@vue/test-utils'
import TourItemBodySummary from '@/components/TourItemBodySummary.vue'

describe('TourItemBodySummary', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TourItemBodySummary)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
