import { mount } from '@vue/test-utils'
import ArticleCardCollection from '../components/ArticleCardCollection'

describe('ArticleCardCollection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ArticleCardCollection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
