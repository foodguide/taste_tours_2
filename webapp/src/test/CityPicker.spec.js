import { mount } from '@vue/test-utils'
import CityPicker from '@/components/CityPicker.vue'

describe('CityPicker', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CityPicker)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
