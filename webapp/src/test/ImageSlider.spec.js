import { mount } from '@vue/test-utils'
import ImageSlider from '@/components/ImageCarousel.vue'

describe('ImageSlider', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ImageSlider)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
