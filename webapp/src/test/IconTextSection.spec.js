import { mount } from '@vue/test-utils'
import IconTextSection from '@/components/IconTextSection.vue'

describe('IconTextSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(IconTextSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
