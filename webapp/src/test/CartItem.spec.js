import { mount } from '@vue/test-utils'
import CartItem from '@/components/CartItem.vue'

describe('CartItem', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CartItem)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
