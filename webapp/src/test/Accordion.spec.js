import { mount } from '@vue/test-utils'
import Accordion from '@/components/Accordion.vue'

describe('Accordion', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(Accordion)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
