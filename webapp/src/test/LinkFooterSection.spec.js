import { mount } from '@vue/test-utils'
import LinkFooterSection from '@/components/LinkFooterSection.vue'

describe('LinkFooterSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(LinkFooterSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
