import { mount } from '@vue/test-utils'
import CheckoutOrder from '@/components/CheckoutOrder.vue'

describe('CheckoutOrder', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CheckoutOrder)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
