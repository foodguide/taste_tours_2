import { mount } from '@vue/test-utils'
import KnownByLogo from '@/components/KnownByLogo.vue'

describe('KnownByLogo', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(KnownByLogo)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
