import { mount } from '@vue/test-utils'
import TourItemBodyDetails from '@/components/TourItemBodyDetails.vue'

describe('TourItemBodyDetails', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TourItemBodyDetails)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
