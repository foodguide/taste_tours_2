import { mount } from '@vue/test-utils'
import TitleSubtitle from '@/components/TitleSubtitle.vue'

describe('TitleSubtitle', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TitleSubtitle)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
