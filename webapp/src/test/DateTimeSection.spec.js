import { mount } from '@vue/test-utils'
import DateTimeSection from '@/components/DateTimeSection.vue'

describe('DateTimeSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(DateTimeSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
