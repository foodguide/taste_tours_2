import { mount } from '@vue/test-utils'
import InfoBox from '@/components/InfoBox.vue'

describe('InfoBox', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(InfoBox)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
