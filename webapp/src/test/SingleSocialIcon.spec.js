import { mount } from '@vue/test-utils'
import SingleSocialIcon from '@/components/SingleSocialIcon.vue'

describe('SingleSocialIcon', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(SingleSocialIcon)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
