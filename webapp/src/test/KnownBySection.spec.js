import {mount} from '@vue/test-utils'
import KnownBySectionVue from '@/components/KnownBySectionVue.vue'

describe('KnownBySectionVue', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(KnownBySectionVue)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})