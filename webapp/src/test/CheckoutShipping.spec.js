import { mount } from '@vue/test-utils'
import CheckoutShipping from '@/components/CheckoutShipping.vue'

describe('CheckoutShipping', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CheckoutShipping)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
