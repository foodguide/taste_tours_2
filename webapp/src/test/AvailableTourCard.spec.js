import { mount } from '@vue/test-utils'
import AvailableTourCard from '@/components/AvailableTourCard.vue'

describe('AvailableTourCard', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(AvailableTourCard)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
