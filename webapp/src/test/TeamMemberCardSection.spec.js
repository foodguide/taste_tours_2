import { mount } from '@vue/test-utils'
import TeamMemberCardSection from '@/components/TeamMemberCardSection.vue'

describe('TeamMemberCardSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TeamMemberCardSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
