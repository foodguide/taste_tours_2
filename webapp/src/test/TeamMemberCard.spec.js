import {mount} from '@vue/test-utils'
import TeamMemberCard from '@/components/TeamMemberCard.vue'

describe('TeamMemberCard', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(TeamMemberCard)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})