import { mount } from '@vue/test-utils'
import ArticleCard from '../components/ArticleCard'

describe('ArticleCard', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ArticleCard)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
