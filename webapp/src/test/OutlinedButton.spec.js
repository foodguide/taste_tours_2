import { mount } from '@vue/test-utils'
import OutlinedButton from '@/components/OutlinedButton.vue'

describe('OutlinedButton', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(OutlinedButton)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
