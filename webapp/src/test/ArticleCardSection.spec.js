import { mount } from '@vue/test-utils'
import ArticleCardSection from '../components/ArticleCardSection'

describe('ArticleCardSection', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ArticleCardSection)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
