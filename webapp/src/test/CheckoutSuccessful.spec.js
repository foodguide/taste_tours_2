import { mount } from '@vue/test-utils'
import CheckoutSuccessful from '@/components/CheckoutSuccessful.vue'

describe('CheckoutSuccessful', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CheckoutSuccessful)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
