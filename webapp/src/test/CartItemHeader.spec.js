import { mount } from '@vue/test-utils'
import CardItemHeader from '@/components/CardItemHeader.vue'

describe('CardItemHeader', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(CardItemHeader)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
