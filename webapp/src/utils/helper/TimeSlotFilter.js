import ComposableFilter from './ComposableFilter'

export class AvailabilityTimeSlotFilter extends ComposableFilter {
  constructor() {
    super((timeSlots) => timeSlots.filter((slot) => !!slot.A && slot.A > 0))
  }
}
