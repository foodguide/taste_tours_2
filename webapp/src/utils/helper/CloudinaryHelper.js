export default class CloudinaryHelper {
  static getBaseUrl() {
    return 'https://res.cloudinary.com/foodiscovr/image/upload'
  }

  static getSVGUrl({
    name,
    color,
    height,
    width,
    isSupportingWebp,
    shouldColorize = true
  }) {
    const start = CloudinaryHelper.getBaseUrl()
    const colorPart = shouldColorize
      ? `co_rgb:${color},e_colorize:100`
      : `co_rgb:${color}`
    const filter = color
      ? `c_scale,${colorPart},h_${height},w_${width}`
      : `c_scale,h_${height},w_${width}`
    let folderPart = 'v1566455642/Taste%20Tours'
    if (name === 'google' || name === 'yelp' || name === 'tripadvisor') {
      folderPart = 'v1575630812/Taste%20Tours'
    } else if (name === 'tt-logo') {
      folderPart = 'v1578067802/Taste%20Tours'
    } else if (['nut', 'cash', 'spoonfork', 'calendar'].includes(name)) {
      folderPart = 'v1582037343/Taste%20Tours'
    }

    const format = CloudinaryHelper.getFormat(isSupportingWebp)
    return [start, filter, folderPart, name].join('/') + format
  }

  static isCloudinaryUrl(url) {
    const pattern = /res.cloudinary.com\/foodiscovr\/image\/upload/
    return pattern.test(url)
  }

  static getHeightPattern() {
    return /h_\d+/
  }

  static getWidthPattern() {
    return /w_\d+/
  }

  static getScaleOptionPattern() {
    return /c_\w+/
  }

  static getColorOptionPattern() {
    return /co_rgb/
  }

  static getOptimizePattern() {
    return /q_auto:eco/
  }

  static hasHeightSpecification(url) {
    return CloudinaryHelper.getHeightPattern().test(url)
  }

  static getSubstringBeforeMatch(str, match) {
    return str.substr(0, match.index)
  }

  static getSubstringAfterMatch(str, match) {
    return str.substr(match.index + match[0].length, str.length)
  }

  static getSubstringAtMatchStart(str, match) {
    return str.substr(match.index, str.length)
  }

  static hasWidthSpecification(url) {
    return CloudinaryHelper.getWidthPattern().test(url)
  }

  static replace(str, pattern, replacement) {
    const match = pattern.exec(str)
    if (!match) {
      return str
    }
    return (
      CloudinaryHelper.getSubstringBeforeMatch(str, match) +
      replacement +
      CloudinaryHelper.getSubstringAfterMatch(str, match)
    )
  }

  static insertBefore(str, pattern, strToInsert) {
    const match = pattern.exec(str)
    if (!match) return str
    return (
      CloudinaryHelper.getSubstringBeforeMatch(str, match) +
      strToInsert +
      CloudinaryHelper.getSubstringAtMatchStart(str, match)
    )
  }

  static hasParameterPart(url) {
    const pattern = /\/upload\/.*\/v\d+\//
    return pattern.test(url)
  }

  static hasScaleOptionPart(url) {
    const pattern = CloudinaryHelper.getScaleOptionPattern()
    return pattern.test(url)
  }

  static hasColorOptionPart(url) {
    const pattern = CloudinaryHelper.getColorOptionPattern()
    return pattern.test(url)
  }

  static hasOptimizeOptionPart(url) {
    const pattern = CloudinaryHelper.getOptimizePattern()
    return pattern.test(url)
  }

  static getUrlWithSize(
    url,
    size,
    isSupportingWebp,
    scaleOption = null,
    colorOption = null
  ) {
    return CloudinaryHelper.getUrlWithResolution(
      url,
      size,
      size,
      isSupportingWebp,
      scaleOption,
      colorOption
    )
  }

  static getUrlWithResolution(
    url,
    width,
    height,
    isSupportingWebp,
    scaleOption = null,
    colorOption = null,
    skipSuffixOptimization = false
  ) {
    if (!CloudinaryHelper.isCloudinaryUrl(url)) return null
    let newUrl = url
    const newHeight = `h_${height}`
    const newWidth = `w_${width}`
    const vPattern = /\/v\d+/
    if (CloudinaryHelper.hasParameterPart(url)) {
      if (CloudinaryHelper.hasHeightSpecification(newUrl)) {
        const hPattern = CloudinaryHelper.getHeightPattern()
        newUrl = CloudinaryHelper.replace(newUrl, hPattern, newHeight)
      } else {
        newUrl = CloudinaryHelper.insertBefore(
          newUrl,
          vPattern,
          `,${newHeight},c_fill`
        )
      }
      if (CloudinaryHelper.hasWidthSpecification(newUrl)) {
        const wPattern = CloudinaryHelper.getWidthPattern()
        newUrl = CloudinaryHelper.replace(newUrl, wPattern, newWidth)
      } else {
        newUrl = CloudinaryHelper.insertBefore(
          newUrl,
          vPattern,
          `,${newWidth},c_fill`
        )
      }
    } else {
      newUrl = CloudinaryHelper.insertBefore(
        newUrl,
        vPattern,
        `/${newWidth},${newHeight},c_fill`
      )
    }
    if (scaleOption) {
      newUrl = CloudinaryHelper.getUrlWithScaleOption(
        scaleOption,
        newUrl,
        vPattern
      )
    }
    if (colorOption) {
      newUrl = CloudinaryHelper.getUrlWithColorOption(
        colorOption,
        newUrl,
        vPattern
      )
    }
    return skipSuffixOptimization
      ? newUrl
      : CloudinaryHelper.getOptimizedUrl(newUrl, vPattern, isSupportingWebp)
  }

  static getUrlWithScaleOption(scaleOption, url, vPattern) {
    let newUrl = url
    const scalePattern = CloudinaryHelper.getScaleOptionPattern()
    const replacement = `c_${scaleOption}`
    if (CloudinaryHelper.hasScaleOptionPart(newUrl)) {
      newUrl = CloudinaryHelper.replace(newUrl, scalePattern, replacement)
    } else {
      newUrl = CloudinaryHelper.insertBefore(newUrl, vPattern, replacement)
    }
    return newUrl
  }

  static getUrlWithColorOption(colorOption, url, vPattern) {
    let newUrl = url
    const colorPattern = CloudinaryHelper.getColorOptionPattern()
    const replacement = `,co_rgb:${colorOption},e_colorize:100`
    if (CloudinaryHelper.hasColorOptionPart(newUrl)) {
      newUrl = CloudinaryHelper.replace(newUrl, colorPattern, replacement)
    } else {
      newUrl = CloudinaryHelper.insertBefore(newUrl, vPattern, replacement)
    }
    return newUrl
  }

  static getOptimizedUrl(url, vPattern, isSupportingWebp) {
    if (CloudinaryHelper.hasOptimizeOptionPart(url)) return url
    const replacement = ',q_auto:eco'
    const format = CloudinaryHelper.getFormat(isSupportingWebp)
    let newUrl = CloudinaryHelper.insertBefore(url, vPattern, replacement)
    newUrl = newUrl.replace('.jpg', format)
    // TODO: dont workaround bugs - write them out (later when there is time)
    newUrl = newUrl.replace(',,', ',')
    return newUrl.replace('.png', format)
  }

  static getFormat(isSupportingWebp) {
    return isSupportingWebp ? '.webp' : '.jp2'
  }
}

export const CloudinaryPresets = {
  thumb: 'thumb',
  original: 'original',
  fullWidth: 'fullWidth'
}
