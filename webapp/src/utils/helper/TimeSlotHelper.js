import { AvailabilityTimeSlotFilter } from './TimeSlotFilter'
import TimeSlotSorter from './TimeSlotSorter'

export default class TimeSlotHelper {
  static filterAvailableTimeSlots(timeSlots) {
    const filter = new AvailabilityTimeSlotFilter()
    return TimeSlotSorter.sortByTime(filter.filter(timeSlots))
  }

  static isSoldOut(slot) {
    return TimeSlotHelper.getAvailableTickets(slot) === 0
  }

  static getAvailableTickets(slot) {
    if (!slot || !slot.A)
      throw new Error(
        'Timeslot does not contain the information about the available tickets.'
      )
    return slot.A
  }

  static setCorrectTimeInSLIP(item, timeSlot) {
    const { slip } = item.rate
    const partToBeUsedInstead = timeSlot.start_time
    const timeRegex = /\d{2}:\d{2}/
    const partToBeReplaced = timeRegex.exec(slip)[0]
    return slip.replace(partToBeReplaced, partToBeUsedInstead)
  }
}
