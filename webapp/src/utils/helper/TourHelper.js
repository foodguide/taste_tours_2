import { MomentHelperFactory } from './MomentHelper'
import {
  FutureDateFilter,
  MonthDateFilter,
  ScheduledToursDateFilter,
  StockAvailabilityDateFilter
} from './DateFilter'
import DateSorter from './DateSorter'
import ItemHelper from './ItemHelper'

export const TourHelperFactory = {
  create: async (i18n) => {
    const momentHelper = await MomentHelperFactory.create(i18n)
    return new TourHelper(momentHelper)
  }
}

export default class TourHelper {
  constructor(momentHelper) {
    this.momentHelper = momentHelper
  }

  fetchTimesForDate(store, date, cityId, articleId) {
    const dateString = this.momentHelper.getDateFormattedString(date.moment)
    return TourHelper.fetchTimesForDateString({
      store,
      dateString,
      cityId,
      articleId
    })
  }

  static fetchTimesForDateString({
    store,
    dateString,
    articleId,
    cityId,
    ticketParameter,
    discountCode = null,
    numberOfTickets = 1
  }) {
    return store.dispatch('tour/fetchTimes', {
      date: dateString,
      cityId,
      articleId,
      discountCode,
      numberOfTickets,
      ticketParameter
    })
  }

  async findNextDateWithAvailableTickets({ month, year }, dates) {
    const filter = new MonthDateFilter({
      month,
      year,
      momentHelper: this.momentHelper
    })
    filter.addFilter(new StockAvailabilityDateFilter())
    filter.addFilter(new FutureDateFilter(this.momentHelper))
    const filtered = filter.filter(dates)
    const sorted = await DateSorter.sortByDate(filtered)
    if (sorted.length === 0) return null
    return sorted[0]
  }

  static hasAvailableStock(date) {
    return TourHelper.getAvailableStockCount(date) > 0
  }

  static getAvailableStockCount(date) {
    return date.stock.A
  }

  static getDatesWithTours(dates) {
    const filter = new ScheduledToursDateFilter()
    return filter.filter(dates)
  }

  getFirstDate(tours) {
    const dates = this.getDates(tours)
    if (!dates || dates.length === 0) return null
    return dates[0]
  }

  getDates(tours) {
    if (!tours) throw new Error('Tours parameter is not valid')
    const items = ItemHelper.getItems(tours)
    return this.getDatesFromItems(items)
  }

  getDatesFromItems(items) {
    let dates = []
    const validItems = TourHelper.getItemsWithValidDates(items)
    validItems.forEach((item) => {
      dates = dates.concat(this.getValidCustomizedDates(item))
    })
    return dates
  }

  static getItemsWithValidDates(items) {
    return items.filter((item) => !!item.rate && !!item.rate.dates)
  }

  getValidCustomizedDates(item) {
    const dates = TourHelper.reorderDatesAsArray(item)
    return dates
      .map((date) => {
        const moment = this.momentHelper.getMomentWithCheckfrontDate(date.date)
        return moment.isValid() ? { moment, ...date } : null
      })
      .filter((date) => !!date)
  }

  static reorderDatesAsArray(item) {
    const dates = []
    Object.keys(item.rate.dates).forEach((dateKey) => {
      dates.push({
        date: dateKey,
        ...item.rate.dates[dateKey]
      })
    })
    return dates
  }

  findDateForCalendarDay(day, dates) {
    const dayMoment = this.momentHelper.getMomentFromCalendarDay(day)
    return TourHelper.findDateForMoment(dayMoment, dates)
  }

  static findDateForMoment(dayMoment, dates) {
    return dates.find((date) => date.moment.isSame(dayMoment, 'day'))
  }
}
