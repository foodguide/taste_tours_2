import ItemHelper from './ItemHelper'
import HTMLEntityHelper from './HTMLEntityHelper'

const entityHelper = new HTMLEntityHelper()

export default class VoucherHelper {
  static sanitizeResult(result) {
    let items = ItemHelper.getItems([result])
    items = items.filter(({ visibility }) => visibility === '*')
    result.items = ItemHelper.transformItemArrayToObject(items)
    return result
  }

  static getPrize(voucher, checkfrontVoucherResult) {
    if (!voucher.articleId)
      throw new Error(
        `ArticleId not found for voucher: ${JSON.stringify(voucher)}`
      )
    try {
      let prize = entityHelper.decode(
        checkfrontVoucherResult.items[voucher.articleId].rate.summary.details
      )
      prize = prize.replace(',00', '')
      prize = prize.replace('.00', '')
      return prize
    } catch (e) {
      throw new Error('Bad checkfront result')
    }
  }
}
