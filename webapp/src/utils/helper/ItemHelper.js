import DiscountHelper from './DiscountHelper'

export default class ItemHelper {
  static getItems(results) {
    const items = []
    results.forEach((result) => {
      if (!result.items) {
        if (!result.item) return null
        items.push({
          id: result.item.item_id,
          ...result.item
        })
        return
      }
      Reflect.ownKeys(result.items).forEach((itemKey) => {
        items.push({
          id: itemKey,
          ...result.items[itemKey]
        })
      })
    })
    return items
  }

  static transformItemArrayToObject(items) {
    const itemsObject = {}
    items.forEach((item) => {
      itemsObject[item.id] = item
    })
    return itemsObject
  }

  static getSelectedTicketsFromItem(item) {
    if (!ItemHelper.isItemRated(item))
      throw new Error(
        'Item is not rated (in ItemHelper/getSelectedTicketsFromItem)'
      )
    return item.qty ? item.qty : item.rate.qty
  }

  static isItemRated(item) {
    if (!item) throw new Error('Item is not rated because it is null.')
    return !!item.rated
  }

  static getTicketParameter(item) {
    return Object.keys(item.rate.summary.price.param)[0]
  }

  static hasItemDiscountCode(item) {
    if (!item || !item.discount) return false
    return !!item.discount.type
  }

  static getDiscountCodeDescription(item) {
    if (!ItemHelper.hasItemDiscountCode(item)) return ''
    const discount = parseFloat(item.discount.flat_discount)
      .toFixed(2)
      .replace('.', ',')
    if (DiscountHelper.isFixedPriceDiscount(item.discount))
      return discount + ' €'
    if (DiscountHelper.isPercentageDiscount(item.discount))
      return discount + '%'
    return ''
  }
}
