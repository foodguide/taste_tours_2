import { AllHtmlEntities as Entities } from 'html-entities'

export default class HTMLEntityHelper {
  constructor() {
    this.entities = new Entities()
  }

  decode(string) {
    return this.entities.decode(string)
  }
}
