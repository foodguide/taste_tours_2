export default {
  primary: 'e84f41',
  success: '3bd77b',
  light: 'ffffff',
  dark: 'f7f7f7',
  white: 'ffffff',
  black: '000000',
  gray: '7f7f7f',
  lightgray: 'd3d3d3'
}
