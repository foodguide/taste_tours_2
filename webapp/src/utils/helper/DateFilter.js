import ComposableFilter from './ComposableFilter'
import TourHelper from './TourHelper'

export class StockAvailabilityDateFilter extends ComposableFilter {
  constructor() {
    super(async (dates) => {
      if (dates.then) dates = await dates
      return dates.filter((date) => TourHelper.hasAvailableStock(date))
    })
  }
}

export class ScheduledToursDateFilter extends ComposableFilter {
  constructor() {
    super(async (dates) => {
      if (dates.then) dates = await dates
      return dates.filter((date) => date.timeslots.length > 0)
    })
  }
}

export class FutureDateFilter extends ComposableFilter {
  constructor(momentHelper) {
    super(async (dates) => {
      if (dates.then) dates = await dates
      const now = momentHelper.getNow()
      return dates.filter((date) => date.moment.isAfter(now, 'day'))
    })
  }
}

export class MonthDateFilter extends ComposableFilter {
  constructor({ month = null, year = null, momentHelper }) {
    super(async (dates) => {
      if (dates.then) dates = await dates
      const monthMoment = momentHelper.getMomentForPage({
        month: this.month,
        year: this.year
      })
      return dates.filter(({ moment }) => moment.isSame(monthMoment, 'month'))
    })
    this.month = month
    this.year = year
  }
}
