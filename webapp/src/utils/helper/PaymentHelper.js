export default class PaymentHelper {
  static getCheckoutUrlAnchor(gateway) {
    if (gateway === PAYMENT_GATEWAY.CREDIT_CARD) return '#payment'
    return '#content'
  }

  static getPaymentUrl({ host, bookingId, token, gateway }) {
    const begin = `https://${host}/reserve/`
    const bookingPage = `${begin}booking/${bookingId}?CFX=${token}&view=pay`
    if (gateway === PAYMENT_GATEWAY.CREDIT_CARD) {
      return `${bookingPage}#payment`
    } else if (gateway === PAYMENT_GATEWAY.PAYPAL) {
      return `${bookingPage}&gateway_id=Paypal&action=connect`
    } else if (gateway === PAYMENT_GATEWAY.GIFT_CERT) {
      return `${begin}giftcert/apply/?booking_id=${bookingId}&CFX=${token}&action=`
    } else if (gateway === PAYMENT_GATEWAY.INVOICE) {
      return `${bookingPage}#mobile-invoice-header`
    }
    return `https://${host}/reserve/booking/${bookingId}?CFX=${token}&view=pay#content`
  }

  static getItemBookingUrl({ host, itemId }) {
    return `https://${host}/reserve/?item_id=${itemId}&popup=1`
  }
}

export const PAYMENT_GATEWAY = {
  CREDIT_CARD: 'credit-card',
  PAYPAL: 'paypal',
  GIFT_CERT: 'gift-cert',
  INVOICE: 'invoice'
}
