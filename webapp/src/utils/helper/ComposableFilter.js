export default class ComposableFilter {
  constructor(filterFunction) {
    this.filterFunction = filterFunction
    this.filters = []
  }

  addFilter(filter) {
    this.filters.push(filter)
  }

  filter(values) {
    let filtered = values
    this.filters.forEach((filter) => {
      filtered = filter.filter(filtered)
    })
    return this.filterFunction(filtered)
  }
}
