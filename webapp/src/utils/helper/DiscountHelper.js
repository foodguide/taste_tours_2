export default class DiscountHelper {
  static calculateDiscountedTotalPrice(discount, totalPrice) {
    const flatDiscount = discount.flat_discount
    const totalPriceFloat = DiscountHelper.getTotalPriceAsFloat(totalPrice)
    if (DiscountHelper.isFixedPriceDiscount(discount))
      return totalPriceFloat - flatDiscount
    if (DiscountHelper.isPercentageDiscount(discount))
      return totalPriceFloat - totalPriceFloat * (flatDiscount / 100)
    throw new Error('Unsupported discount mode')
  }

  static getDiscountedTotalPriceDescription(discount, totalPrice) {
    const discounted = DiscountHelper.calculateDiscountedTotalPrice(
      discount,
      totalPrice
    )
    const currency = DiscountHelper.getCurrency(totalPrice)
    return `${discounted.toFixed(2)} ${currency}`
  }

  static getCurrency(totalPrice) {
    const splitted = totalPrice.replace(/\s/g, ' ').split(' ')
    return splitted[splitted.length - 1]
  }

  static getTotalPriceAsFloat(totalPrice) {
    return DiscountHelper.transformFloatStringToFloat(totalPrice)
  }

  static transformFloatStringToFloat(floatString) {
    return parseFloat(/\d+[,.]\d{2}/.exec(floatString)[0].replace(',', '.'))
  }

  static isPercentageDiscount(discount) {
    return discount.mode === 'P'
  }

  static isFixedPriceDiscount(discount) {
    return discount.mode === 'F'
  }
}
