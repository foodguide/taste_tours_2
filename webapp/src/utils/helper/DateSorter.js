export default class DateSorter {
  static async sortByDate(dates) {
    if (dates.then) dates = await dates
    return dates.sort(
      (left, right) => left.moment.toDate() - right.moment.toDate()
    )
  }
}
