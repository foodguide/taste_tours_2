export default class ProductHelper {
  static getPrize(checkFrontResult) {
    try {
      let prize = checkFrontResult.item.rate.sub_total
      prize = prize.replace(',00', '')
      prize = prize.replace('.00', '')
      return prize
    } catch (e) {
      throw new Error('Bad checkfront result')
    }
  }

  static getProductLink(component, citySlug, productSlug) {
    return component.localePath({
      name: 'cities-city-tours-product',
      params: {
        city: citySlug,
        product: productSlug
      }
    })
  }
}
