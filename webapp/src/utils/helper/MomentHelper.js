export const MomentHelperFactory = {
  create: (i18n) => {
    return new Promise((resolve) => {
      import('moment').then((moment) => {
        resolve(new MomentHelper(moment, i18n))
      })
    })
  }
}

export default class MomentHelper {
  constructor(moment, i18n) {
    this.moment = moment
    if (i18n) this.moment.locale(i18n.locale)
  }

  getDateFormattedStringFromCheckfrontDate(date) {
    const moment = this.getMomentWithCheckfrontDate(date)
    return MomentHelper.getDateFormattedString(moment)
  }

  getMomentFromStandard(date) {
    return this.moment.utc(date)
  }

  getMomentWithCheckfrontDate(date) {
    return this.moment.utc(date, MomentHelper.getCheckfrontDateFormat())
  }

  isPageNowOrInFuture(page) {
    const pageMoment = this.getMomentForPage(page)
    return pageMoment.isSameOrAfter(this.getCurrentMonthMoment())
  }

  getMomentForPage({ month, year }) {
    return this.moment.utc({ year, month: month - 1 }).startOf('month')
  }

  getMomentForDateFormatString(dateString) {
    return this.moment.utc(dateString, MomentHelper.getDateFormat())
  }

  getCurrentMonthMoment() {
    return this.getNow().startOf('month')
  }

  getNow() {
    return this.moment.utc()
  }

  static getDateFormattedString(moment) {
    return moment.format(MomentHelper.getDateFormat())
  }

  static getDateFormat() {
    return `${MomentHelper.getMonthFormat()}-DD`
  }

  static getMonthFormat() {
    return 'YYYY-MM'
  }

  static getCheckfrontDateFormat() {
    return 'YYYYMMDD'
  }

  static getCityPickerFormattedString(moment) {
    if (!moment || moment.then) return null
    return moment.format(MomentHelper.getCityPickerDateFormat())
  }

  static getCityPickerDateFormat() {
    return 'DD MMM'
  }

  getTourTimeDescription(timeSlot, duration) {
    const format = 'HH:mm'
    const endTime = this.moment
      .utc(timeSlot.start_time, format)
      .add(duration, 'hour')
    return `${timeSlot.start_time} - ${endTime.format(format)}`
  }

  getDateFormattedStringFromTimeSlot(timeSlot) {
    const moment = this.getMomentFromTimeSlot(timeSlot)
    return MomentHelper.getDateCheckoutFormattedString(moment)
  }

  getMomentFromTimeSlot(timeSlot) {
    return this.moment.utc(timeSlot.start_date * 1000)
  }

  getMomentFromCalendarDay({ id }) {
    return this.moment.utc(id, MomentHelper.getDateFormat())
  }

  static getDateCheckoutFormattedString(moment) {
    return moment.format(MomentHelper.getDateCheckoutFormat())
  }

  static getDateCheckoutFormat() {
    return 'dddd, DD. MMM YYYY'
  }

  static getDateTitleFormattedString(moment) {
    return moment.format(MomentHelper.getDateTitleFormat())
  }

  static getDateTitleFormat() {
    return 'DD. MMMM YYYY'
  }

  isTodayOrInFuture(moment) {
    return this.getNow()
      .subtract(1, 'days')
      .isBefore(moment, 'day')
  }
}
