// const slugify = () => import('slugify')
import slugify from 'slugify'

export default class SnippetHelper {
  static t(component, snippet) {
    if (!snippet) {
      throw new Error(
        `No snippet given. Component: ${component._name}. parent: ${component.$parent._name}`
      )
    }
    return snippet[component.$i18n.locale]
  }

  static tWithi18n(i18n, snippet) {
    if (!snippet) {
      throw new Error(`No snippet given.`)
    }
    return snippet[i18n.locale]
  }

  static tReplace(component, snippet, replacements) {
    let text = SnippetHelper.t(component, snippet)
    Reflect.ownKeys(replacements).forEach((replace) => {
      const toReplace = '{{' + replace + '}}'
      text = text.split(toReplace).join(replacements[replace])
    })
    return text
  }

  static tByIdentifier(
    component,
    identifier,
    replacements = null,
    useStyledSnippets = false
  ) {
    const snippet = SnippetHelper.findSnippet(
      component,
      identifier,
      useStyledSnippets
    )
    if (!snippet) return null
    if (replacements && Reflect.ownKeys(replacements).length > 0) {
      return SnippetHelper.tReplace(component, snippet, replacements)
    }
    if (identifier === 'empty') return ''
    return SnippetHelper.t(component, snippet)
  }

  static getSnippets(component, useStyledSnippets = false) {
    return useStyledSnippets
      ? component.$store.state.styledsnippet.styledsnippets
      : component.$store.state.snippet.snippets
  }

  static findSnippet(component, identifier, useStyledSnippets = false) {
    const snippets = SnippetHelper.getSnippets(component, useStyledSnippets)
    return SnippetHelper.find(snippets, identifier)
  }

  static find(snippets, identifier) {
    return snippets.find((snippet) => snippet.identifier === identifier)
  }

  static getSlugForLocation(component, { name }) {
    if (!name) return null
    return slugify(name).toLowerCase()
  }

  static getSlugForCity(component, city) {
    if (!city.name) return null
    return slugify(SnippetHelper.t(component, city.name)).toLowerCase()
  }

  static getNameForCity(component, city) {
    if (!city.name) return null
    return SnippetHelper.t(component, city.name)
  }

  static findCityForSlug(component, cities, slug) {
    if (!slug || !cities) return null
    return cities.find(
      (city) => SnippetHelper.getSlugForCity(component, city) === slug
    )
  }

  static findLocationForSlug(component, locations, city, slug) {
    if (!slug || !city || !locations) return null
    const filteredByCity = locations.filter(
      (location) => location.city.id === city.id
    )
    return filteredByCity.find(
      (location) =>
        SnippetHelper.getSlugForLocation(component, location) === slug
    )
  }

  static getSlug(component, object, prop) {
    if (!object[prop]) return null
    return slugify(SnippetHelper.t(component, object[prop])).toLowerCase()
  }

  static findObjectForSlug(component, objects, slug, prop) {
    if (!slug) return null
    return objects.find(
      (object) => SnippetHelper.getSlug(component, object, prop) === slug
    )
  }
}
