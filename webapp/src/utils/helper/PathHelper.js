export default class PathHelper {
  static getCartLink(component, booking, citySlug) {
    if (!booking || !booking.session) return component.localePath('index')
    return component.localePath(
      PathHelper.getCartPath(component, booking, citySlug)
    )
  }

  static getCartPath(component, booking, citySlug) {
    const sessionId = booking.session.id
    return {
      name: `cities-city-cart-sessionId`,
      params: {
        sessionId,
        city: citySlug
      }
    }
  }
}
