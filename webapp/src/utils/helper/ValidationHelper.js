export default class ValidationHelper {
  static isValidPhoneNumber(input) {
    let isValid = true
    input.split('').forEach((c) => {
      if (
        c === ' ' ||
        c === '-' ||
        c === '/' ||
        c === '+' ||
        (c >= 0 && c <= 9)
      ) {
        // do nothing in here!
      } else {
        isValid = false
      }
    })
    return isValid
  }
}
