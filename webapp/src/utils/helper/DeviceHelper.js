export const DeviceHelperFactory = {
  create: () => {
    return new Promise((resolve) => {
      import('detect-browser').then((detectBrowser) => {
        resolve(new DeviceHelper(detectBrowser))
      })
    })
  }
}

export default class DeviceHelper {
  constructor(detectBrowser) {
    this.detectBrowser = detectBrowser
  }

  isSafari = () => this.detectBrowser.detect().name === 'safari'
}
