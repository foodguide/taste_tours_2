import pluralize from 'pluralize'

export default class StoreHelper {
  static createGraphQLModule({ name, client, query, additional }) {
    const generatorData = {
      client,
      query,
      pluralized: pluralize(name)
    }
    if (additional) {
      generatorData.additionalFields = additional.state
      generatorData.additionalGetters = additional.getters
      generatorData.additionalMutations = additional.mutations
      generatorData.additionalActions = additional.actions
    }
    return {
      state: StoreHelper.createState(generatorData),
      getters: StoreHelper.createGetters(generatorData),
      mutations: StoreHelper.createMutations(generatorData),
      actions: StoreHelper.createGraphQLActions(generatorData)
    }
  }

  static createState({ pluralized, additionalFields }) {
    return () => {
      return {
        [pluralized]: [],
        ...additionalFields
      }
    }
  }

  static createGetters({ pluralized, additionalGetters = {} }) {
    return {
      get: (state) => state[pluralized],
      ...additionalGetters
    }
  }

  static createMutations({ pluralized, additionalMutations = {} }) {
    return {
      set: (state, data) => (state[pluralized] = data),
      ...additionalMutations
    }
  }

  static createGraphQLActions({ pluralized, query, additionalActions = {} }) {
    return {
      fetch: ({ commit }, { client }) => {
        client
          .query({
            query
          })
          .then(({ data }) => {
            commit(StoreConstants.SET, data[pluralized])
          })
      },
      ...additionalActions
    }
  }
}

export const StoreModules = {
  CITY: 'city',
  SNIPPET: 'snippet',
  STYLED_SNIPPET: 'styledsnippet',
  CATEGORY: 'category',
  TOUR: 'tour',
  KEY_VALUE: 'keyvalue',
  IMAGE: 'image'
}

export const StoreFunctions = {
  GET: 'get',
  SET: 'set',
  FETCH: 'fetch'
}

export const StoreConstants = {
  modules: StoreModules,
  ...StoreFunctions,
  combine(module, functionality) {
    return `${module}/${functionality}`
  }
}
