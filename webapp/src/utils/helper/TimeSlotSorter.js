export default class TimeSlotSorter {
  static sortByTime(timeSlots) {
    return timeSlots.sort((left, right) =>
      left.start_time.localeCompare(right.start_time)
    )
  }
}
