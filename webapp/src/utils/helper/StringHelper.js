export default class StringHelper {
  static capitalize([head, ...tail]) {
    return [head.toUpperCase(), ...tail].join('')
  }
}
