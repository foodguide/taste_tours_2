import TourHelper, { TourHelperFactory } from '../helper/TourHelper'

export default {
  props: {
    cityId: {
      type: Number,
      required: true
    }
  },
  computed: {
    tours() {
      return this.$store.getters['tour/toursForCity']({ cityId: this.cityId })
    },
    calendar() {
      return this.$store.getters['tour/calendarForArticle']({
        cityId: this.cityId,
        articleId: this.articleId
      })
    }
  },
  methods: {
    async dates() {
      const tourHelper = await TourHelperFactory.create(this.$i18n)
      return tourHelper.getDates(this.tours)
    },
    async datesWithTours() {
      return TourHelper.getDatesWithTours(await this.dates())
    }
  }
}
