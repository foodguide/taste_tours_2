import slugify from 'slugify'
import SnippetHelper from '../helper/SnippetHelper'
import CloudinaryHelper from '../helper/CloudinaryHelper'

const seoImageWidth = 1800
const seoImageHeight = 942

const getOptimizedImageUrl = (url) => {
  return CloudinaryHelper.getUrlWithResolution(
    url,
    seoImageWidth,
    seoImageHeight,
    false,
    null,
    null,
    true
  )
}

const getSeoInfoForLocation = async (context) => {
  const cityForPage = await getCityForPage(context)
  const locationForPage = await getLocationForPage(context, cityForPage)

  return {
    seoInfo: {
      title: locationForPage.name,
      description: SnippetHelper.tWithi18n(
        context.app.i18n,
        locationForPage.description
      ),
      image: getOptimizedImageUrl(locationForPage.image.url)
    }
  }
}

const getSeoInfoForCity = async (context) => {
  const cityForPage = await getCityForPage(context)

  return {
    seoInfo: {
      title: SnippetHelper.tWithi18n(context.app.i18n, cityForPage.seoTitle),
      description: SnippetHelper.tWithi18n(
        context.app.i18n,
        cityForPage.seoDescription
      ),
      image: getOptimizedImageUrl(cityForPage.image.url)
    }
  }
}

const getSeoInfoForCityRestaurantPage = async (context) => {
  const cityForPage = await getCityForPage(context)

  return {
    seoInfo: {
      title: SnippetHelper.tWithi18n(
        context.app.i18n,
        cityForPage.restaurantSeoTitle
      ),
      description: SnippetHelper.tWithi18n(
        context.app.i18n,
        cityForPage.restaurantSeoDescription
      )
    }
  }
}

const getSeoInfoForStaticPage = async (context, pageIdentifier) => {
  const identifier = pageIdentifier || 'index'
  const { data: seoInfos } = await context.$axios.get(`/api/seoinfos`)
  const seoInfoForPage = seoInfos.find(
    (seoInfo) => seoInfo.identifier === identifier
  )

  return seoInfoForPage
    ? {
        seoInfo: {
          title: SnippetHelper.tWithi18n(
            context.app.i18n,
            seoInfoForPage.title
          ),
          description: SnippetHelper.tWithi18n(
            context.app.i18n,
            seoInfoForPage.description
          ),
          image: getOptimizedImageUrl(seoInfoForPage.image.image.url)
        }
      }
    : null
}

const getCityForPage = async (context) => {
  const { data: cities } = await context.$axios.get(`/api/cities`)
  return cities.find((city) => {
    return (
      slugify(
        SnippetHelper.tWithi18n(context.app.i18n, city.name)
      ).toLowerCase() === context.route.params.city
    )
  })
}

const getLocationForPage = async (context, cityForPage) => {
  const { data: locations } = await context.$axios.get(
    `/api/locations?city=${cityForPage.id}`
  )
  return locations.find((location) => {
    return slugify(location.name).toLowerCase() === context.route.params.name
  })
}

export default {
  data() {
    return {
      seoInfo: null
    }
  },
  async asyncData(context) {
    try {
      const route = context.route

      if (route.path.includes('/restaurants/')) {
        // Like: '/cities/frankfurt/restaurants/bullys-burger'
        return await getSeoInfoForLocation(context)
      } else if (route.params.city && route.path.endsWith(route.params.city)) {
        // Like: '/cities/frankfurt'
        return await getSeoInfoForCity(context)
      } else if (route.params.city && route.path.endsWith('/restaurants')) {
        // Like: '/cities/frankfurt/restaurants'
        return await getSeoInfoForCityRestaurantPage(context)
      }

      const lastPathPart = route.path.split('/').slice(-1)[0]
      const seoInfoForStaticPage = await getSeoInfoForStaticPage(
        context,
        lastPathPart
      )
      if (seoInfoForStaticPage) {
        return seoInfoForStaticPage
      }
    } catch (e) {}
  },
  head() {
    return this.seoInfo
      ? {
          title: this.seoInfo.title,
          meta: [
            {
              hid: 'description',
              name: 'description',
              content: this.seoInfo.description
            },
            {
              hid: 'og:description',
              name: 'og:description',
              content: this.seoInfo.description
            },
            {
              hid: 'og:title',
              name: 'og:title',
              content: this.seoInfo.title
            },
            {
              hid: 'og:site_name',
              name: 'og:site_name',
              content: 'Taste Tours'
            },
            {
              hid: 'og:type',
              name: 'og:type',
              content: 'website'
            },
            {
              hid: 'og:image',
              name: 'og:image',
              content: this.seoInfo.image
            }
          ]
        }
      : {}
  }
}
