import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      host: 'cart/getHost'
    })
  },
  methods: {
    getCityId() {
      throw new Error(
        'This method needs to be overwritten. CheckfrontHostMixin/getCityId'
      )
    },
    fetchHost() {
      const params = {
        cityId: this.getCityId()
      }
      return this.$store.dispatch('cart/fetchHost', params)
    }
  },
  async mounted() {
    if (this.host) return
    await this.fetchHost()
  }
}
