import { mapGetters } from 'vuex'
import CurrencyHelper from '../helper/CurrencyHelper'

export default {
  computed: {
    ...mapGetters({
      booking: 'cart/getBooking',
      cartCityId: 'cart/getCityId',
      host: 'cart/getHost',
      isCartFilled: 'cart/isCartFilled'
    }),
    session() {
      if (!this.booking) return null
      return this.booking.session
    },
    bookingId() {
      if (!this.booking) return null
      return this.booking.id
    },
    hasSessionItems() {
      return this.sessionItems.length > 0
    },
    sessionItems() {
      if (!this.session) return []
      const keys = Object.keys(this.session.item)
      const items = []
      keys.forEach((key) => {
        items.push({
          id: key,
          lineId: key,
          ...this.session.item[key]
        })
      })
      return items
    },
    totalBookedTickets() {
      if (!this.session) return null
      let tickets = 0
      this.sessionItems.forEach((item) => {
        tickets += item.rate.qty
      })
      return tickets
    },
    totalBookingPrice() {
      if (!this.session) return ''
      const price = parseFloat(this.session.total)
      return CurrencyHelper.currencyFormatDE(price)
    },
    gateway() {
      return this.$route.query.gateway
    },
    hasSessionDiscount() {
      return (
        (this.session &&
          this.session.flat_discount &&
          this.session.flat_discount.total &&
          this.session.flat_discount.total > 0) > 0
      )
    }
  },
  mounted() {
    if (!this.cartCityId) {
      this.$store.commit('cart/setCityId', this.getCityId())
    }
  },
  methods: {
    getCityId() {
      throw new Error(
        'This method needs to be overwritten. CartMixin/getCityId'
      )
    },
    fetchBookingSession() {
      const params = {
        cityId: this.getCityId(),
        sessionId: this.$route.params.sessionId
      }
      return this.$store.dispatch('cart/fetchBookingSession', params)
    },
    fetchBooking() {
      const params = {
        cityId: this.getCityId(),
        bookingId: this.$route.params.bookingId
      }
      return this.$store.dispatch('cart/fetchBooking', params)
    },
    fetchHost() {
      const params = {
        cityId: this.getCityId()
      }
      return this.$store.dispatch('cart/fetchHost', params)
    },
    mountedBookingIdPage() {
      this.$nextTick(async () => {
        if (!this.booking) {
          await this.fetchBooking()
        }
        if (!this.host) {
          await this.fetchHost()
        }
      })
    },
    clearBookingSession() {
      return this.$store.dispatch('cart/clearBookingSession')
    },
    onRemoveItem(sessionItem) {
      if (this.sessionItems.length === 1) {
        return this.clearBookingSession()
      }
      const { lineId } = sessionItem
      return this.$store.dispatch('cart/removeBookingSessionItem', { lineId })
    }
  }
}
