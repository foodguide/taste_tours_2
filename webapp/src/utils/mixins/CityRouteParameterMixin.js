export default {
  computed: {
    isCityParameterIncludedInRoute() {
      return !!this.citySlug
    },
    citySlug() {
      return this.$route.params.city
    }
  }
}
