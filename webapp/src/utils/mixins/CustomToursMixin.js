import query from '../../apollo/queries/customtours.graphql'

export default {
  apollo: {
    customtours: {
      query
    }
  },
  computed: {
    mappedTours() {
      if (!this.customtours) return []
      return this.customtours.map((tour) => {
        return {
          ...tour,
          to: 'tours/custom',
          title: this.tForSnippet(tour.title),
          subtitle: this.tForSnippet(tour.subtitle),
          imageUrl: tour.image.url
        }
      })
    }
  }
}
