export default {
  computed: {
    socialLinks() {
      return [
        {
          type: 'facebook',
          url: this.getValueForKey('link_facebook')
        },
        {
          type: 'instagram',
          url: this.getValueForKey('link_instagram')
        }
      ]
    }
  }
}
