import SnippetHelper from '../helper/SnippetHelper'

export default {
  methods: {
    tSnippet(identifier, replacements = null, useStyledSnippets = false) {
      return SnippetHelper.tByIdentifier(
        this,
        identifier,
        replacements,
        useStyledSnippets
      )
    },
    tForSnippet(snippet) {
      return SnippetHelper.t(this, snippet)
    },
    getSlugForCity(city) {
      return SnippetHelper.getSlugForCity(this, city)
    },
    findCityForSlug(cities, slug) {
      return SnippetHelper.findCityForSlug(this, cities, slug)
    },
    getNameForCity(city) {
      return SnippetHelper.getNameForCity(this, city)
    },
    getPathForCity(city) {
      const name = this.getSlugForCity(city)
      return this.localePath({
        name: 'cities-city',
        params: { city: name }
      })
    },
    getPathForJob(job, citySlug) {
      const jobSlug = this.getSlug(job, 'title')
      let name = 'cities-city-jobs-job'
      let params = { city: citySlug, job: jobSlug }
      if (!citySlug) {
        name = 'jobs-job'
        params = { job: jobSlug }
      }
      return this.localePath({
        name,
        params
      })
    },
    getSlug(obj, prop) {
      return SnippetHelper.getSlug(this, obj, prop)
    },
    getPathForLocation(city, location) {
      const cityName = this.getSlugForCity(city)
      const locationName = SnippetHelper.getSlugForLocation(this, location)
      return this.localePath({
        name: 'cities-city-restaurants-name',
        params: { city: cityName, name: locationName }
      })
    },
    findLocationForSlug(slug, city, locations) {
      return SnippetHelper.findLocationForSlug(this, locations, city, slug)
    },
    findObjectForSlug(slug, objects, prop) {
      return SnippetHelper.findObjectForSlug(this, objects, slug, prop)
    }
  }
}
