import { MomentHelperFactory } from '../helper/MomentHelper'

export default {
  computed: {
    dateString() {
      if (!this.$route || !this.$route.params || !this.$route.params.date)
        return null
      return this.$route.params.date
    }
  },
  methods: {
    async getMoment() {
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      return this.dateString
        ? momentHelper.getMomentForDateFormatString(this.dateString)
        : null
    }
  }
}
