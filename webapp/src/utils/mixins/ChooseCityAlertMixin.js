import { interopDefault } from '../../.nuxt/utils'

const Swal = () => interopDefault(import('sweetalert2'))

export default {
  methods: {
    async showChooseCityAlert(snippet = 'choose_city_voucher_disabled') {
      const swal = await Swal()
      const text = this.tSnippet(snippet)
      swal.fire({
        text,
        confirmButtonColor: '#e84f41'
      })
    }
  }
}
