import { mapGetters } from 'vuex'
import TourHelper, { TourHelperFactory } from '../helper/TourHelper'
import MomentHelper from '../helper/MomentHelper'
import TimeSlotHelper from '../helper/TimeSlotHelper'
import ItemHelper from '../helper/ItemHelper'

export default {
  data: () => {
    return {
      isLoading: true,
      timesResult: null
    }
  },
  computed: {
    ...mapGetters({
      cartItem: 'cart/getItem',
      cartTimeSlot: 'cart/getTimeSlot'
    })
  },
  methods: {
    async getNextTimeSlot() {
      const timeSlots = await this.getTimeSlots()
      if (!timeSlots || timeSlots.length === 0) return null
      return timeSlots[0]
    },
    async getTimeSlots() {
      if (!this.timesResult) return null
      const tourHelper = await TourHelperFactory.create(this.$i18n)
      const firstDate = tourHelper.getFirstDate([this.timesResult])
      if (!firstDate) return null
      return TimeSlotHelper.filterAvailableTimeSlots(firstDate.timeslots)
    },
    fetchTimesForDate(obj, cityId, articleId) {
      const { moment } = obj
      if (!moment) return null
      return this.fetchTimesForMoment(moment, cityId, articleId)
    },
    fetchTimesForMoment(moment, cityId, articleId) {
      const dateString = MomentHelper.getDateFormattedString(moment)
      return this.fetchTimes({ dateString, cityId, articleId })
    },
    async fetchTimes({
      dateString,
      cityId,
      articleId,
      discountCode = null,
      numberOfTickets = 1,
      ticketParameter = null
    }) {
      this.isLoading = true
      this.timesResult = await TourHelper.fetchTimesForDateString({
        store: this.$store,
        dateString,
        cityId,
        discountCode,
        numberOfTickets,
        ticketParameter,
        articleId
      })
      return new Promise((resolve) => {
        this.$nextTick(() => {
          this.setCart(this.timesResult, cityId)
          this.isLoading = false
          resolve()
        })
      })
    },
    setCart(timesResult, cityId) {
      if (!timesResult) return null
      const item = ItemHelper.getItems([timesResult])[0]
      this.$store.commit('cart/setCityId', cityId)
      this.$store.commit('cart/setItem', item)
      this.$store.commit('cart/setTimeSlot', this.nextTimeSlot)
    }
  }
}
