import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      keyvalues: 'keyvalue/get'
    })
  },
  methods: {
    getValueForKey(key) {
      return this.keyvalues.find((keyvalue) => keyvalue.identifier === key)
        .value
    }
  }
}
