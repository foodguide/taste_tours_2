import ProductHelper from '../helper/ProductHelper'
import SnippetHelper from '../helper/SnippetHelper'

export default {
  data() {
    return {
      mappedProductsWithPrizes: []
    }
  },
  watch: {
    async products() {
      const mappedProducts = this.getMappedProducts()
      this.mappedProductsWithPrizes = await this.fetchAndMapProductPrizes(
        mappedProducts
      )
    }
  },
  methods: {
    async fetchAndMapProductPrizes(products) {
      const productsWithPrizes = [...products]
      const articleIds = this.getArticleIdsForProducts(products)

      await Promise.all(
        articleIds.map(async ({ productId, articleId }) => {
          const result = await this.getCheckfrontResultForArticleId(articleId)
          const productIndex = productsWithPrizes.findIndex(
            (product) => product.id === productId
          )
          productsWithPrizes[productIndex].prize = ProductHelper.getPrize(
            result
          )
        })
      )
      return productsWithPrizes
    },
    getArticleIdsForProducts(products) {
      const articleIds = []
      products.forEach((product) => {
        const { articleId } = this.getArticleConnectionForProduct(product)
        articleIds.push({
          articleId,
          productId: product.id
        })
      })
      return articleIds
    },
    getArticleConnectionForProduct(product) {
      return product.articleConnections.find((articleConnection) => {
        return (
          articleConnection.city.id === this.getCityId() &&
          articleConnection.state === 'enabled'
        )
      })
    },
    async getCheckfrontResultForArticleId(articleId) {
      const result = await this.$store.dispatch('product/fetch', {
        cityId: this.getCityId(),
        articleId
      })
      return result
    },
    getMappedProducts() {
      if (!this.products) return []
      return this.products
        .filter((product) => {
          return product.articleConnections.some((articleConnection) => {
            return (
              articleConnection.city.id === this.getCityId() &&
              articleConnection.state === 'enabled'
            )
          })
        })
        .sort((product1, product2) => {
          return product1.order - product2.order
        })
    },
    findProductFromRoute(products) {
      const slug = this.$route.params.product
      if (!slug)
        throw new Error(
          "This function ProductMixin.findProductWithRoute() can only be used from a page that has the product in it's path."
        )
      return SnippetHelper.findObjectForSlug(this, products, slug, 'name')
    }
  }
}
