import { MomentHelperFactory } from '../helper/MomentHelper'

export default {
  props: {
    sessionItem: {
      type: Object,
      required: true
    }
  },
  computed: {
    singlePrice() {
      return parseInt(this.sessionItem.rate.item_total)
    },
    totalPrice() {
      return parseInt(this.sessionItem.rate.total)
    },
    selectedTickets() {
      return parseInt(this.sessionItem.rate.qty)
    },
    mappedTimeSlot() {
      return {
        ...this.sessionItem.date,
        A: this.sessionItem.available - this.sessionItem.rate.qty
      }
    }
  },
  methods: {
    async getDate() {
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      return momentHelper.getDateFormattedStringFromTimeSlot(
        this.sessionItem.date
      )
    },
    async getTime() {
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      return momentHelper.getTourTimeDescription(this.sessionItem.date, 4)
    }
  }
}
