import { MomentHelperFactory } from '../helper/MomentHelper'
import TimeSlotHelper from '../helper/TimeSlotHelper'
import HTMLEntityHelper from '../helper/HTMLEntityHelper'
import ItemHelper from '../helper/ItemHelper'
import DiscountHelper from '../helper/DiscountHelper'

const entityHelper = new HTMLEntityHelper()

export default {
  props: {
    timeSlot: {
      type: Object,
      required: true
    },
    item: {
      type: Object,
      required: true
    }
  },
  computed: {
    singlePrice() {
      this.checkItem()
      return entityHelper.decode(this.item.rate.summary.price.title)
    },
    totalPrice() {
      this.checkItem()
      return entityHelper.decode(this.item.rate.summary.price.total)
    },
    availableTickets() {
      return TimeSlotHelper.getAvailableTickets(this.timeSlot)
    },
    selectedTickets() {
      return ItemHelper.getSelectedTicketsFromItem(this.item)
    },
    isSoldOut() {
      return TimeSlotHelper.isSoldOut(this.timeSlot)
    },
    hasDiscount() {
      return ItemHelper.hasItemDiscountCode(this.item)
    },
    discountDescription() {
      return ItemHelper.getDiscountCodeDescription(this.item)
    },
    discountedTotalPrice() {
      return DiscountHelper.getDiscountedTotalPriceDescription(
        this.item.discount,
        this.totalPrice
      )
    },
    ticketParameter() {
      return ItemHelper.getTicketParameter(this.item)
    }
  },
  watch: {
    async timeSlot() {
      await this.calculateDateTime()
    }
  },
  async mounted() {
    await this.calculateDateTime()
  },
  methods: {
    checkItem() {
      if (!this.item.rated)
        throw new Error('Passed item to TimeSlotBodyMixin is not a rated item')
    },
    async getDateFromTimeSlot() {
      if (!this.timeSlot) return null
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      return momentHelper.getDateFormattedStringFromTimeSlot(this.timeSlot)
    },
    async getTimeFromTimeSlot() {
      if (!this.timeSlot) return null
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      return momentHelper.getTourTimeDescription(this.timeSlot, 4)
    },
    async calculateDateTime() {
      this.time = await this.getTimeFromTimeSlot()
      this.date = await this.getDateFromTimeSlot()
    }
  }
}
