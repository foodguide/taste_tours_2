import { mapGetters } from 'vuex'
import CloudinaryHelper from '../helper/CloudinaryHelper'
import { DeviceHelperFactory } from '../helper/DeviceHelper'

export default {
  props: {
    identifier: {
      type: String,
      default: null
    },
    width: {
      type: Number,
      default: null
    },
    height: {
      type: Number,
      default: null
    },
    size: {
      type: Number,
      default: null
    }
  },
  computed: {
    ...mapGetters({
      images: 'image/get'
    }),
    imageOptions() {
      return {
        identifier: this.identifier,
        isMobile: this.$device.isMobile,
        imageObj: this.getImageObject(),
        size: this.size,
        width: this.width,
        height: this.height
      }
    }
  },
  methods: {
    getImageObject() {
      throw new Error('getImageObject needs to be overwritten')
    },
    async getImage({ identifier, isMobile, imageObj, size, width, height }) {
      const url = identifier
        ? this.getImageFromIdentifier(isMobile, identifier)
        : this.getImageFromObject(isMobile, imageObj)
      if (!url) return url
      if (!size && !width && !height) return url
      if (size) {
        height = size
        width = size
      }
      if (!this.$device.isMobile) {
        height *= 2
        width *= 2
      }
      const deviceHelper = await DeviceHelperFactory.create()
      const isSafari = deviceHelper.isSafari()
      return CloudinaryHelper.getUrlWithResolution(
        url,
        width,
        height,
        !this.$device.isIos && !isSafari
      )
    },
    handleImageNotFound(identifier, isMobile) {
      return identifier !== 'placeholder'
        ? this.getImage({ identifier: 'placeholder', isMobile })
        : null
    },
    getImageFromObject(isMobile, { image, mobileImage }) {
      return isMobile && mobileImage ? mobileImage.url : image.url
    },
    getImageFromIdentifier(isMobile, identifier) {
      const image = this.images.find((img) => img.identifier === identifier)
      if (!image) return this.handleImageNotFound(identifier, isMobile)
      return isMobile && image.mobile ? image.mobile.url : image.image.url
    }
  }
}
