import { MomentHelperFactory } from '../helper/MomentHelper'

export default {
  methods: {
    async fetchTours(month, cityId, articleId) {
      await this.$store.dispatch('tour/fetchTours', {
        cityId,
        month,
        articleId
      })
    },
    async fetchCalendar(month, cityId, articleId) {
      await this.$store.dispatch('tour/fetchCalendar', {
        month,
        cityId,
        articleId
      })
    },
    getMonthFromPage({ month, year }) {
      return `${year}-${month}`
    },
    getNextMonthFromPage({ month, year }) {
      if (month === 12) {
        return this.getMonthFromPage({
          month: 1,
          year: year + 1
        })
      }
      return this.getMonthFromPage({
        year,
        month: month + 1
      })
    },
    getPreviousMonthFromPage({ month, year }) {
      if (month === 1) {
        return this.getMonthFromPage({
          month: 12,
          year: year - 1
        })
      }
      return this.getMonthFromPage({
        year,
        month: month - 1
      })
    },
    async fetchCurrentMonthFromPage(page, cityId, articleId) {
      const month = this.getMonthFromPage(page)
      await this.fetchCalendar(month, cityId, articleId)
      await this.fetchTours(month, cityId, articleId)
    },
    async fetchNextMonthFromPage(page, cityId, articleId) {
      const month = this.getNextMonthFromPage(page)
      await this.fetchCalendar(month, cityId, articleId)
      await this.fetchTours(month, cityId, articleId)
    },
    async fetchPreviousMonthFromPage(page, cityId, articleId) {
      const momentHelper = await MomentHelperFactory.create(this.$i18n)
      if (!momentHelper.isPageNowOrInFuture(page)) return Promise.resolve()
      const month = this.getPreviousMonthFromPage(page)
      const calendar = this.$store.getters['tour/calendarForMonth']({
        month,
        cityId: this.cityId,
        articleId: this.articleId
      })
      if (calendar) return
      await this.fetchCalendar(month, cityId, articleId)
      await this.fetchTours(month, cityId, articleId)
    }
  }
}
