export default {
  methods: {
    onIFrameURLChange(href) {},
    // credits going out to: https://gist.github.com/hdodov/a87c097216718655ead6cf2969b0dcfa
    listenIFrameURLChange(iframe) {
      let lastDispatched = null
      const dispatchChange = () => {
        const newHref = iframe.contentWindow.location.href
        if (newHref !== lastDispatched) {
          this.onIFrameURLChange(newHref)
          lastDispatched = newHref
        }
      }

      const unloadHandler = () => {
        // Timeout needed because the URL changes immediately after
        // the `unload` event is dispatched.
        setTimeout(dispatchChange, 0)
      }

      const attachUnload = () => {
        // Remove the unloadHandler in case it was already attached.
        // Otherwise, there will be two handlers, which is unnecessary.
        iframe.contentWindow.removeEventListener('unload', unloadHandler)
        iframe.contentWindow.addEventListener('unload', unloadHandler)
      }

      iframe.addEventListener('load', () => {
        attachUnload()
        // Just in case the change wasn't dispatched during the unload event...
        dispatchChange()
      })

      attachUnload()
    }
  }
}
