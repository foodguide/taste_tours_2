import PathHelper from '../helper/PathHelper'

export default {
  computed: {
    links() {
      const isCartFilled = this.$store.getters['cart/isCartFilled']
      const booking = this.$store.getters['cart/getBooking']
      const cartPath = isCartFilled
        ? PathHelper.getCartPath(this, booking, this.citySlug)
        : 'index'
      const toursPath = this.citySlug
        ? this.pathForCitySubpage(this.citySlug, 'tours')
        : 'tours'
      return [
        {
          snippet: 'cart',
          to: cartPath,
          isActive: () => this.$store.getters['cart/isCartFilled'],
          order: 200,
          notActiveMessage: 'cart_is_empty'
        },
        {
          snippet: 'our_tours',
          to: toursPath,
          isActive: () => true,
          order: 30
        },
        {
          snippet: 'vouchers',
          order: 20,
          to: this.pathForCitySubpage(this.citySlug, 'vouchers'),
          isActive: () => !!this.citySlug,
          notActiveMessage: 'please_choose_city_first'
        },
        {
          snippet: 'restaurants',
          to: this.pathForCitySubpage(this.citySlug, 'restaurants'),
          order: 10,
          isActive: () => !!this.citySlug,
          notActiveMessage: 'please_choose_city_first'
        },
        {
          snippet: 'about',
          to: this.citySlug
            ? this.pathForCitySubpage(this.citySlug, 'about')
            : 'about',
          order: 180,
          isActive: () => true
        }
      ]
    },
    sortedLinks() {
      // clone before sort to prevent side effect (manipulating the filteredLinks computed var)
      return Array.prototype.slice
        .call(this.links)
        .sort((left, right) => left.order - right.order)
    }
  }
}
