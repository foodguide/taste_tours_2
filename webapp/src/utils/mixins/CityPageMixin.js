import { mapGetters } from 'vuex'
import SnippetHelper from '../helper/SnippetHelper'

export default {
  computed: {
    cityId() {
      return this.$route.params.city
    },
    city() {
      return SnippetHelper.findCityForSlug(
        this,
        this.cities,
        this.$route.params.city
      )
    },
    cityName() {
      return SnippetHelper.t(this, this.city.name)
    },
    citySlug() {
      return this.$route.params.city
    },
    ...mapGetters({
      cities: 'city/get'
    })
  },
  methods: {
    pathForCitySubpage(citySlug, pageName) {
      return {
        name: `cities-city-${pageName}`,
        params: {
          city: citySlug
        }
      }
    },
    pathForCity(citySlug) {
      return {
        name: 'cities-city',
        params: {
          city: citySlug
        }
      }
    },
    homePath() {
      return this.citySlug ? this.pathForCity(this.citySlug) : { name: 'index' }
    },
    localeHomePath() {
      return this.localePath(this.homePath())
    },
    getCityId() {
      return this.city.id
    }
  }
}
