import { MomentHelperFactory } from '../utils/helper/MomentHelper'
import TimeSlotHelper from '../utils/helper/TimeSlotHelper'

export const state = () => {
  return {
    cityId: null,
    item: null,
    timeSlot: null,
    booking: null,
    host: null,
    checkoutLabels: {}
  }
}

export const getters = {
  get(state) {
    return {
      item: state.item,
      timeSlot: state.timeSlot
    }
  },
  getItem(state) {
    return state.item
  },
  getTimeSlot(state) {
    return state.timeSlot
  },
  getCityId(state) {
    return state.cityId
  },
  getBooking(state) {
    return state.booking
  },
  getHost(state) {
    return state.host
  },
  isCartFilled(state) {
    return (
      !!state.booking &&
      !!state.booking.session &&
      !!state.booking.session.item &&
      !!Object.keys(state.booking.session.item).length > 0
    )
  }
}

export const mutations = {
  setItem(state, item) {
    state.item = item
  },
  setTimeSlot(state, timeSlot) {
    state.timeSlot = timeSlot
  },
  setCityId(state, cityId) {
    state.cityId = cityId
  },
  setBooking(state, booking) {
    state.booking = booking
  },
  setHost(state, host) {
    state.host = host
  }
}

export const actions = {
  async update(context, numberOfTickets) {
    const momentHelper = await MomentHelperFactory.create(context.$i18n)
    const { cityId, item } = getCheckedUpdateValues(context)
    const itemId = item.item_id
    const date = momentHelper.getDateFormattedStringFromCheckfrontDate(
      item.rate.start_date
    )
    const param = Object.keys(item.rate.summary.price.param)[0]
    const url = `api/cities/${cityId}/tours/${itemId}/${date}/${param}/${numberOfTickets}`
    const result = await this.$axios.$get(url)
    context.commit('setItem', result.item)
    return result
  },
  async createBookingSession(context, { timeSlot }) {
    const { cityId, item } = getCheckedUpdateValues(context)
    const url = `api/cities/${cityId}/booking-sessions`
    const slip = TimeSlotHelper.setCorrectTimeInSLIP(item, timeSlot)
    const params = { slip }
    const { booking } = context.state
    if (booking && booking.session) params.sessionId = booking.session.id
    const result = await this.$axios.$post(url, params)
    context.commit('setBooking', result.booking)
  },
  async alterBookingSessionItem(context, { lineId, amount }) {
    let cityId = null
    let item = null
    if (lineId && amount) cityId = getCheckedCityId(context.state)
    else {
      const values = getCheckedUpdateValues(context)
      cityId = values.cityId
      item = values.item
    }
    const url = `api/cities/${cityId}/booking-sessions`
    const params = {}
    if (item) params.slip = item.rate.slip
    if (lineId && amount) {
      params.alterOptions = {
        lineId,
        amount
      }
    }
    const { booking } = context.state
    if (booking && booking.session) params.sessionId = booking.session.id
    const result = await this.$axios.$post(url, params)
    context.commit('setBooking', result.booking)
  },
  removeBookingSessionItem(context, { lineId }) {
    return context.dispatch('alterBookingSessionItem', {
      lineId,
      amount: 'remove' // Found in the checkfront api
    })
  },
  async createBooking(context, formData) {
    const { cityId, booking } = getCheckedBookingValues(context)
    const params = {
      formData,
      sessionId: booking.session.id
    }
    const url = `api/cities/${cityId}/bookings`
    const result = await this.$axios.$post(url, params)
    context.commit('setBooking', result.booking)
  },
  async fetchBookingSession(context, { cityId, sessionId }) {
    const url = `api/cities/${cityId}/booking-sessions/${sessionId}`
    const result = await this.$axios.$get(url)
    context.commit('setBooking', result.booking)
  },
  async fetchBooking(context, { cityId, bookingId }) {
    const url = `api/cities/${cityId}/bookings/${bookingId}`
    const result = await this.$axios.$get(url)
    context.commit('setBooking', result.booking)
  },
  clearBookingSession(context) {
    const { cityId, booking } = getCheckedBookingValues(context)
    const url = `api/cities/${cityId}/booking-sessions/${booking.session.id}`
    const result = this.$axios.$delete(url)
    context.commit('setBooking', result.booking)
  },
  async fetchHost(context, { cityId }) {
    const url = `api/cities/${cityId}/host`
    const { host } = await this.$axios.$get(url)
    context.commit('setHost', host)
  },
  async applyGiftCert({ host, bookingId, token, code }) {
    const data = {
      CFX: token,
      booking_id: bookingId,
      action: '',
      payment_notify: '1',
      gcn: code
    }
    const url = `https://${host}/reserve/giftcert/apply/`
    await this.$axios({
      url,
      data,
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
  },
  async getCheckoutLabels({ state }) {
    const cityId = getCheckedCityId(state)
    const key = `city-${cityId}`
    if (state.checkoutLabels[key]) return state.checkoutLabels[key]
    const url = `api/cities/${cityId}/checkout-labels`
    const result = await this.$axios.$get(url)
    state.checkoutLabels[key] = result
    return result
  }
}

const getCheckedCityId = (state) => {
  const cityId = state.cityId
  if (!cityId) throw new Error('Cannot update cart item. CityId is missing')
  return cityId
}

const getCheckedUpdateValues = ({ state }) => {
  const cityId = getCheckedCityId(state)
  const item = state.item
  if (!item) throw new Error('Cannot update cart item. Item is missing')
  // TODO: readd timeslot again if we know how set the checkfront request to a specific timeslot
  // const timeSlot = state.timeSlot
  // if (!timeSlot) throw new Error('Cannot update cart item. TimeSlot is missing')
  return {
    cityId,
    item
  }
}

const getCheckedBookingValues = ({ state }) => {
  const cityId = getCheckedCityId(state)
  const booking = state.booking
  if (!booking) throw new Error('No booking is set in cart vuex store.')
  return {
    cityId,
    booking
  }
}
