import {
  StoreConstants as c,
  StoreModules as m
} from '../utils/helper/StoreHelper'

const initialFetch = (dispatch, client, modules) => {
  return modules.map((module) => {
    return dispatch(c.combine(module, c.FETCH), { client })
  })
}

export const actions = {
  async nuxtServerInit({ dispatch }, { app }) {
    const client = app.apolloProvider.defaultClient
    await initialFetch(dispatch, client, [
      m.SNIPPET,
      m.STYLED_SNIPPET,
      m.CITY,
      m.CATEGORY,
      m.KEY_VALUE,
      m.IMAGE
    ])
  }
}
