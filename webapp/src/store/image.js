import StoreHelper from '../utils/helper/StoreHelper'
import query from '../apollo/queries/images.graphql'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'image'
})
