import StoreHelper from '../utils/helper/StoreHelper'
import query from '../apollo/queries/keyvalues.graphql'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'keyvalue'
})
