import { MomentHelperFactory } from '../utils/helper/MomentHelper'

export const state = () => {
  return {
    times: [],
    tours: [],
    calendars: []
  }
}
export const getters = {
  tours(state) {
    return state.tours
  },
  times(state) {
    return state.times
  },
  calendars(state) {
    return state.calendars
  },
  calendarForArticle(state) {
    return ({ cityId, articleId }) =>
      state.calendars.filter(
        (cal) => cal.cityId === cityId && cal.articleId === articleId
      )
  },
  toursForCity(state) {
    return ({ cityId }) => state.tours.filter((tour) => tour.cityId === cityId)
  },
  toursForMonth(state, getters) {
    return ({ cityId, month, articleId }) => {
      return getters
        .toursForCity({ cityId })
        .filter((tour) => tour.month === month && tour.articleId === articleId)
    }
  },
  calendarForMonth(state, getters) {
    return ({ cityId, articleId, month }) => {
      state.calendars.filter(
        (cal) =>
          cal.cityId === cityId &&
          cal.articleId === articleId &&
          cal.month === month
      )
    }
  },
  timesForCity(state) {
    return ({ cityId }) => state.times.filter((tour) => tour.cityId === cityId)
  },
  timesForDate(state, getters) {
    return ({ cityId, date, articleId, discountCode, numberOfTickets }) => {
      const key = createKey({
        cityId,
        articleId,
        discountCode,
        numberOfTickets
      })
      return getters
        .timesForCity(key)
        .find((tour) => tour.queryDate === date && tour.articleId === articleId)
    }
  }
}
export const mutations = {
  setTours(state, { cityId, month, articleId, result }) {
    result.month = month
    result.cityId = cityId
    result.articleId = articleId
    state.tours.push(result)
  },
  setTimes(state, { cityId, date, articleId, result, discountCode }) {
    const key = createKey({ cityId, discountCode, articleId })
    result.queryDate = date
    result.cityId = key
    result.articleId = articleId
    state.times.push(result)
  },
  setCalendar(state, { cityId, month, articleId, result }) {
    result.cityId = cityId
    result.month = month
    result.articleId = articleId
    state.calendars.push(result)
  },
  resetCalendars(state) {
    state.calendars = []
  }
}
export const actions = {
  async fetchTours(context, { month, cityId, articleId }) {
    let result = context.getters.toursForMonth({ cityId, month, articleId })
    if (result.length > 0) return result
    const url = `/api/cities/${cityId}/tours/${month}?articleId=${articleId}`
    result = await this.$axios.$get(url)
    context.commit('setTours', { cityId, month, articleId, result })
    return result
  },
  async fetchCalendar(context, { month, cityId, articleId }) {
    const url = `/api/cities/${cityId}/calendar/${month}?articleId=${articleId}`
    const result = await this.$axios.$get(url)
    const momentHelper = await MomentHelperFactory.create(this.$i18n)
    context.commit('setCalendar', {
      cityId,
      month,
      articleId,
      result: result.map((day) => {
        return {
          ...day,
          moment: momentHelper.getMomentFromStandard(day.date)
        }
      })
    })
    return result
  },
  async fetchTimes(
    context,
    { date, cityId, discountCode, numberOfTickets, ticketParameter, articleId }
  ) {
    let result = context.getters.timesForDate({
      cityId,
      date,
      articleId,
      discountCode
    })
    if (result) return result
    let url = `/api/cities/${cityId}/tours/${date}/times`
    let sign = '?'
    if (articleId) {
      url += `${sign}articleId=${articleId}`
      sign = '&'
    }
    if (discountCode) {
      url += `${sign}discount_code=${discountCode}`
      sign = '&'
    }
    if (numberOfTickets && ticketParameter) {
      url += `${sign}number_of_tickets=${numberOfTickets}&ticket_parameter=${ticketParameter}`
    }
    result = await this.$axios.$get(url)
    context.commit('setTimes', {
      cityId,
      date,
      articleId,
      discountCode,
      numberOfTickets,
      result
    })
    return result
  }
}

const createKey = (obj) => {
  const keys = Object.keys(obj)
  let combined = ''
  keys.forEach((key) => {
    const value = obj[key]
    combined += `${key}-${value}-`
  })
  return combined
}
