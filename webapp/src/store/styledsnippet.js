import query from '../apollo/queries/styledsnippets.graphql'
import StoreHelper from '../utils/helper/StoreHelper'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'styledsnippet'
})
