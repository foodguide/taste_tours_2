export const state = () => {}

export const getters = {}

export const mutations = {}

export const actions = {
  async fetch(context, { cityId, articleId }) {
    const url = `/api/cities/${cityId}/items/${articleId}`
    const result = await this.$axios.$get(url)
    return result
  }
}
