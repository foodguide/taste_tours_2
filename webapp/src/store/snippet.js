import query from '../apollo/queries/snippets.graphql'
import StoreHelper from '../utils/helper/StoreHelper'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'snippet'
})
