export const state = () => {
  return {
    month: null,
    year: null
  }
}

export const getters = {
  getMonth(state) {
    return state.month
  },
  getYear(state) {
    return state.year
  }
}

export const mutations = {
  update(state, { month, year }) {
    state.month = month
    state.year = year
  }
}

export const actions = {}
