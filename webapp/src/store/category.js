import StoreHelper from '../utils/helper/StoreHelper'
import query from '../apollo/queries/categories.graphql'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'category'
})
