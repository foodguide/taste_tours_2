import StoreHelper from '../utils/helper/StoreHelper'
import query from '../apollo/queries/cities.graphql'

export default StoreHelper.createGraphQLModule({
  query,
  name: 'city',
  additional: {
    state: {
      selectedCity: null
    },
    getters: {
      getSelectedcity: (state) => state.selectedCity
    },
    mutations: {
      setSelectedCity: (state, city) => {
        state.selectedCity = city
      }
    }
  }
})
