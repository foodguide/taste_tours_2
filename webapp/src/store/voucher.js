import VoucherHelper from '../utils/helper/VoucherHelper'

export const state = () => {
  return {
    vouchers: []
  }
}

export const getters = {
  get(state) {
    return state.vouchers
  },
  getForCity(state) {
    return ({ cityId }) =>
      state.vouchers.filter((tour) => tour.cityId === cityId)
  }
}

export const mutations = {
  set(state, { cityId, result }) {
    result.cityId = cityId
    state.vouchers.push(result)
  }
}

export const actions = {
  async fetch(context, { cityId }) {
    const url = `/api/cities/${cityId}/vouchers`
    let result = await this.$axios.$get(url)
    result = VoucherHelper.sanitizeResult(result)
    context.commit('set', { cityId, result })
    return result
  }
}
