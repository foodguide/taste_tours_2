const apiUrl = process.env.API_BASE_URL
  ? process.env.API_BASE_URL
  : 'http://localhost:1337'
const browserApiUrl = process.env.API_BASE_URL_BROWSER
  ? process.env.API_BASE_URL_BROWSER
  : 'http://localhost:1337'
const axios = require('axios')
const slugify = require('slugify')

const availableLocales = [
  {
    code: 'en',
    file: 'en.js'
  },
  {
    code: 'de',
    file: 'de.js'
  }
]
const defaultLocale = 'de'

module.exports = {
  mode: 'universal',
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'Taste Tours',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'preload',
        as: 'style',
        href:
          'https://fonts.googleapis.com/css?family=Source+Sans+Pro:900&family=Roboto:400,700&display=swap'
      },
      {
        href:
          'https://fonts.googleapis.com/css?family=Source+Sans+Pro:900&family=Roboto:400,700&display=swap',
        rel: 'stylesheet'
      },
      {
        rel: 'dns-prefetch',
        href: 'https://www.google-analytics.com'
      },
      {
        rel: 'dns-prefetch',
        href: 'https://www.googletagmanager.com'
      },
      {
        rel: 'preconnect',
        crossorigin: true,
        href: 'https://www.google-analytics.com'
      },
      {
        rel: 'preconnect',
        crossorigin: true,
        href: 'https://www.googletagmanager.com'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [{ src: '~/assets/scss/variables.scss', lang: 'sass' }],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '~/plugins/vue-lazyload.js', ssr: false },
    '~/plugins/vue-awesome-swiper.js',
    '~/plugins/directives.js'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  devModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/apollo',
    [
      'nuxt-i18n',
      {
        locales: availableLocales,
        defaultLocale,
        lazy: true,
        langDir: 'locales/'
      }
    ],
    ['@nuxtjs/google-tag-manager', { id: 'GTM-N7FDZT8', dev: false }],
    '@nuxtjs/pwa',
    'nuxt-purgecss',
    '@nuxtjs/component-cache',
    '@nuxtjs/device',
    '@nuxtjs/sitemap'
  ],

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    proxy: true
  },
  proxy: {
    '/api/': { target: apiUrl, pathRewrite: { '^/api': '' } }
  },
  apollo: {
    errorHandler: '~/apollo/error-handler.js',
    clientConfigs: {
      default: {
        httpEndpoint: `${apiUrl}/graphql`,
        browserHttpEndpoint: `${browserApiUrl}/graphql`
      }
    }
  },
  pwa: {
    workbox: {
      runtimeCaching: [
        {
          urlPattern: 'https://res.cloudinary.com/foodiscovr/*',
          handler: 'cacheFirst'
        }
      ]
    }
  },
  purgeCSS: {
    // enabled: ({ isDev, isClient }) => (!isDev && isClient),
    enabled: () => true,
    paths: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'assets/scss/variables.scss'
    ]
  },
  sitemap: {
    gzip: true,
    routes: async () => {
      const dynamicBaseRoutes = ['/restaurants', '/vouchers', '/tours']
      const { data: cities } = await axios.get(`${apiUrl}/cities`)
      const { data: locations } = await axios.get(`${apiUrl}/locations`)
      const locales = availableLocales.map((locale) => locale.code)

      const dynamicRoutes = []

      locales.forEach((locale) => {
        const isDefaultLocale = locale === defaultLocale
        cities.forEach((city) => {
          const cityNameSlugified = slugify(city.name[locale]).toLowerCase()
          const cityBaseRoute = isDefaultLocale
            ? `cities/${cityNameSlugified}`
            : `${locale}/cities/${cityNameSlugified}`
          dynamicRoutes.push(cityBaseRoute)

          dynamicBaseRoutes.forEach((route) =>
            dynamicRoutes.push(`${cityBaseRoute}${route}`)
          )

          const cityLocationsFilter = (location) =>
            location.city.id === city.id && location.state === 'enabled'
          locations.filter(cityLocationsFilter).forEach((location) => {
            const locationNameSlugified = slugify(location.name).toLowerCase()
            dynamicRoutes.push(
              `${cityBaseRoute}/restaurants/${locationNameSlugified}`
            )
          })
        })
      })

      return dynamicRoutes
    }
  },
  buildModules: [
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-151913138-1'
      }
    ]
  ],
  /*
   ** Build configuration
   */
  build: {
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
}
