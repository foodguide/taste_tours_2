import Vue from 'vue'
import SnippetHelper from '../utils/helper/SnippetHelper'

const render = (text) => {
  return import('markdown-it').then((MarkdownIt) => {
    const md = MarkdownIt.default({ breaks: true, linkify: true })
    return md.render(text)
  })
}

const addTargetBlankForLinks = (text) => {
  return text.replace(/ href/g, ' target="_blank" href')
}

const isSnippetObject = (value) =>
  !!value.identifier || (!!value.de && !!value.en)

const onChange = (el, value, vnode) => {
  if (typeof value === 'object') {
    if (value.text) {
      el.textContent = value.text
      return
    } else if (value.identifier) {
      const replacements = value.replacements ? value.replacements : null
      const text = SnippetHelper.tByIdentifier(
        vnode.context.$parent,
        value.identifier,
        replacements
      )
      if (!text) return
      el.textContent = text
    }
  }
  const text = SnippetHelper.tByIdentifier(vnode.context.$parent, value)
  if (!text) return
  el.textContent = text
}

Vue.directive('snippet', {
  inserted(el, { value }, vnode) {
    onChange(el, value, vnode)
  },
  update(el, { value }, vnode) {
    onChange(el, value, vnode)
  }
})

const onStyledChange = async (el, value, vnode) => {
  const component = vnode.context.$parent
  const text = isSnippetObject(value)
    ? SnippetHelper.t(component, value)
    : SnippetHelper.tByIdentifier(component, value, null, true)
  if (!text) return
  const renderedText = await render(text)
  const htmlText = addTargetBlankForLinks(renderedText)
  el.innerHTML = htmlText
}

Vue.directive('styledsnippet', {
  async inserted(el, { value }, vnode) {
    await onStyledChange(el, value, vnode)
  },
  async update(el, { value }, vnode) {
    await onStyledChange(el, value, vnode)
  }
})

const onObjectChange = (el, value, vnode) => {
  const text = SnippetHelper.t(vnode.context.$parent, value)
  if (!text) return
  el.textContent = text
}

Vue.directive('snippetobject', {
  inserted(el, { value }, vnode) {
    onObjectChange(el, value, vnode)
  },
  update(el, { value }, vnode) {
    onObjectChange(el, value, vnode)
  }
})
