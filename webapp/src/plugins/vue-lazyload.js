import Vue from 'vue'
import VueLazyload from 'vue-lazyload'
import CloudinaryHelper from '../utils/helper/CloudinaryHelper'

const isCDN = (url) => {
  const isCDN = /cloudinary.com\/foodiscovr/
  return isCDN.test(url)
}

const isJPG = (url) => {
  const isJPG = /.jpg/
  return isJPG.test(url)
}

const isPNG = (url) => {
  const isPNG = /.png/
  return isPNG.test(url)
}

Vue.use(VueLazyload, {
  filter: {
    progressive(listener, options) {
      const { src } = listener
      if ((isCDN(src) && isJPG(src)) || isPNG(src)) {
        listener.el.setAttribute('lazy-progressive', 'true')
        const isSupportingWebp = options.supportWebp
        listener.loading = CloudinaryHelper.getUrlWithSize(
          src,
          12,
          isSupportingWebp
        )
      }
    }
  }
})
