#!/bin/sh
set -ea

_stopNuxt() {
  echo "Stopping nuxt"
  kill -SIGINT "$nuxtPID"
  wait "$nuxtPID"
}

trap _stopnuxt SIGTERM SIGINT

cd /usr/src/nuxt

APP_NAME=webapp
CMS_PORT=${CMS_PORT:-1337}
echo "starting nuxt"
cd "$APP_NAME"
if [ ! -d "node_modules" ]
then
    npm install
fi

npm rebuild node-sass
npm run dev &

nuxtPID=$!
wait "$nuxtPID"

