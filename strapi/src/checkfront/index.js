const axios = require('axios');
const queryString = require('query-string');
const urlParse = require('url-parse');

module.exports = {
  CheckfrontApi: class CheckfrontApi {
    constructor(connection) {
      if (!connection) throw new Error('No connection given for initiating the checkfront api');
      this.connection = connection;
      this.axios = axios.create({
        baseURL: connection.endpoint,
      });
    }

    async item(id, params) {
      return await this.fetch(`item/${id}`, params);
    }

    async items(categoryId = null, additionalParams = {}) {
      let params = {...additionalParams};
      if (categoryId) params.category_id = categoryId;
      return await this.fetch('item', params);
    }

    async calendarItems(categoryId, startDate, endDate = null) {
      let params = {
        category_id: categoryId,
        start_date: startDate
      };
      if (endDate) params.end_date = endDate;
      return await this.fetch('item/cal', params);
    }

    async event() {
      return await this.fetch('event');
    }

    async getBookingSession(sessionId) {
      return await this.fetch('booking/session', { session_id: sessionId });
    }

    async getBookingForm() {
      return await this.fetch('booking/form');
    }

    async getBooking(bookingId) {
      return await this.fetch(`booking/${bookingId}`);
    }

    async fetch(url, params = null){
      const paramUrl = params ? `${url}?${queryString.stringify(params)}` : url;
      return await this.axios.get(paramUrl, this.getOptions()).then(result => result.data);
    }

    async createBooking(sessionId, clientIP, formData) {
      if (!sessionId) throw new Error('No session id given for creating a booking.');
      const params = { session_id: sessionId };
      formData.forEach(({ key, value }) => {
        params[`form[${key}]`] = value;
      });
      return this.post('booking/create', params, clientIP);
    }

    async alterBookingSession(slip, sessionId, alterOptions) {
      if (!slip && !sessionId) throw new Error('No data given for the booking session.');
      const params = {};
      if (slip) params.slip = slip;
      if (sessionId) params.session_id = sessionId;
      if (alterOptions) {
        const { lineId, amount } = alterOptions;
        params[`alter[${lineId}]`] = amount;
      }
      return this.post('booking/session', params);
    }

    async deleteBookingSession(sessionId) {
      return this.post('booking/session/clear', {session_id: sessionId});
    }

    async closeBookingSession(sessionId) {
      return this.post('booking/session/end', {session_id: sessionId});
    }

    async post(url, params, clientIP) {
      const bodyFormData = queryString.stringify(params);
      const headers = clientIP ? this.getClientHeaders(clientIP) : this.getHeaders();
      const response = await this.axios({
        url,
        headers,
        method: 'post',
        data: bodyFormData,
        ...this.getOptions()
      });
      return response.data;
    }

    getOptions() {
      return {
        auth: {
          username: this.connection.key,
          password: this.connection.secret
        }
      };
    }

    getHeaders() {
      return {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Host': this.getHost()
      };
    }

    getHost() {
      const parsedEndpoint = urlParse(this.connection.endpoint);
      return parsedEndpoint.host;
    }

    getClientHeaders(clientIP) {
      return {
        ...this.getHeaders(),
        'X-Forwarded-For': clientIP,
        'X-On-Behalf': 'off'
      };
    }
  }
};
