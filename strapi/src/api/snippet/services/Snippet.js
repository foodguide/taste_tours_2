'use strict';
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async find(params, populate) {
    const key = 'snippets';
    const setCallback = () => {
      return strapi.query('snippet').find(params, populate);
    };
    return strapi.services.redis.getOrSet(key, setCallback, 10 * 60);
  },
};
