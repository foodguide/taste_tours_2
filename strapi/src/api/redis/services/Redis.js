'use strict';
const redis = require('redis');
const {promisify} = require('util');
let client = redis.createClient({host: 'redis'});
const getAsync = promisify(client.get).bind(client);

module.exports = {
  async getOrSet(key, setCallback, cacheDuration) {
    let result = await getAsync(key);
    if (!result) {
      result = await setCallback();
      client.set(key, JSON.stringify(result), 'EX', cacheDuration);
    } else {
      result = JSON.parse(result);
    }
    return result;
  }
};
