'use strict';

const getIp = (ctx) => {
  let clientIP = ctx.request.ip;
  let splitted = clientIP.split(':');
  return splitted[splitted.length - 1];
};

const handleError = (ctx, e) => {
  if (e.message === 'No connection given for initiating the checkfront api') {
    ctx.throw(400, e.message);
    return;
  }
  throw e;
};

const doTry = async(ctx, doThis) => {
  try {
    return await doThis();
  } catch (e) {
    handleError(ctx, e);
  }
};

const doBookingAction = async (ctx, actionName) => {
  return await doTry(ctx, async () => {
    return await strapi.services.checkfrontconnection[actionName](ctx.params.id, ctx.params.sessionId);
  });
};

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  tours: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItemsForMonth(ctx.params.id, ctx.params.month, ctx.query);
    });
  },
  calendar: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchCalendarForMonth(ctx.params.id, ctx.params.month, ctx.query);
    });
  },
  customTours: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItems(ctx.params.id, 'custom_tour_category_id');
    });
  },
  packages: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItems(ctx.params.id, 'package_category_id');
    });
  },
  vouchers: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItems(ctx.params.id, 'voucher_category_id', true);
    });
  },
  times: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItemTimes(ctx.params.id, ctx.params.date, ctx.query);
    });
  },
  item: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.fetchItem(ctx.params.id, ctx.params.itemId);
    });
  },
  tickets: async ctx => {
    return await doTry(ctx, async () => {
      const options = {
        cityId: ctx.params.id,
        itemId: ctx.params.itemId,
        date: ctx.params.date,
        ticketParameter: ctx.params.param,
        tickets: ctx.params.tickets
      };
      return await strapi.services.checkfrontconnection.fetchItemTickets(options);
    });
  },
  alterBookingSession: async ctx => {
    return await doTry(ctx, async () => {
      const { slip, sessionId, alterOptions } = ctx.request.body;
      return await strapi.services.checkfrontconnection.alterBookingSession(ctx.params.id, slip, sessionId, alterOptions);
    });
  },
  getBookingSession: async ctx => {
    return await doBookingAction(ctx, 'getBookingSession');
  },
  deleteBookingSession: async ctx => {
    return await doBookingAction(ctx, 'deleteBookingSession');
  },
  closeBookingSession: async ctx => {
    return await doBookingAction(ctx, 'closeBookingSession');
  },
  createBooking: async ctx => {
    const { sessionId, formData } = ctx.request.body;
    const clientIP = getIp(ctx);
    return strapi.services.checkfrontconnection.createBooking(ctx.params.id, sessionId, clientIP, formData);
  },
  getBookingForm: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.getBookingForm();
    });
  },
  getBooking: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.getBooking(ctx.params.id, ctx.params.bookingId);
    });
  },
  getHost: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.getHost(ctx.params.id);
    });
  },
  getCheckoutLabels: async ctx => {
    return await doTry(ctx, async () => {
      return await strapi.services.checkfrontconnection.getCheckoutLabels(ctx.params.id);
    });
  }
};
