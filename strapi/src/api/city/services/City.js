'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/services.html#core-services)
 * to customize this service
 */

const duration = 9 * 60;
module.exports = {
  async find(params, populate) {
    const key = 'cities';
    const setCallback = () => {
      return strapi.query('city').find(params, populate);
    };
    return strapi.services.redis.getOrSet(key, setCallback, duration);
  },
  async findOne(ctx) {
    const key = `city-${encodeURIComponent(JSON.stringify(ctx.params))}`;
    const setCallback = () => {
      return strapi.services.restaurant.findOne(ctx.params);
    };
    return strapi.services.redis.getOrSet(key, setCallback, duration);
  },
};
