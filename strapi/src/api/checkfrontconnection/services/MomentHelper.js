const moment = require('moment');

module.exports = class MomentHelper {
  static getMomentForMonth(month) {
    return month ? moment.utc(month, MomentHelper.getMonthFormat()) : moment.utc().startOf('month');
  }

  static transformDateToCheckfront(date) {
    const moment = MomentHelper.getMomentForDate(date);
    return MomentHelper.getCheckfrontDate(moment);
  }

  static transformCheckfrontToDate(checkfrontDate) {
    const moment = MomentHelper.transformCheckfrontToMoment(checkfrontDate);
    return moment.toDate();
  }

  static transformCheckfrontToMoment(checkfrontDate) {
    const format = MomentHelper.getCheckfrontDateFormat();
    return checkfrontDate ? moment.utc(checkfrontDate, format) : moment.utc().startOf('day');
  }

  static getNowAsCheckfrontDate() {
    const now = moment.utc();
    return MomentHelper.getCheckfrontDate(now);
  }

  static getNow() {
    return moment.utc();
  }

  static isCheckfrontDateInPast(date) {
    const checkfrontMoment = MomentHelper.transformCheckfrontToMoment(date);
    const now = MomentHelper.getNow();
    return !now.isSameOrBefore(checkfrontMoment, 'day');
  }

  static getCheckfrontDate(moment) {
    return moment.format(MomentHelper.getCheckfrontDateFormat());
  }

  static getMomentForDate(date) {
    return date ? moment.utc(date,  MomentHelper.getDateFormat()) : moment.utc().startOf('day');
  }

  static getCheckfrontDateFormat(){
    return 'YYYYMMDD';
  }

  static getMonthFormat() {
    return 'YYYY-MM';
  }

  static getDateFormat() {
    return 'YYYY-MM-DD';
  }
};
