'use strict';
const MomentHelper = require('./MomentHelper');
const { CheckfrontApi } = require('./../../../checkfront');

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/services.html#core-services)
 * to customize this service
 */

const duration = 9 * 60;
module.exports = {
  async findConnection(cityId) {
    const cityKey = `city-${cityId}`;
    const setCityCallback = () => {
      return strapi.query('city').findOne({ id: cityId });
    };
    const city = await strapi.services.redis.getOrSet(cityKey, setCityCallback, duration);
    const key = `checkfrontconnection-${encodeURIComponent(city.checkfrontName)}`;
    const setCallback = () => {
      return strapi.query('checkfrontconnection').findOne({ name: city.checkfrontName });
    };
    return strapi.services.redis.getOrSet(key, setCallback, duration);
  },

  async getApi(cityId) {
    const connection = await this.findConnection(cityId);
    return new CheckfrontApi(connection);
  },

  async fetchItem(cityId, id) {
    const api = await this.getApi(cityId);
    const params = {
      start_date: MomentHelper.getNowAsCheckfrontDate()
    };
    return await api.item(id, params);
  },

  async fetchItems(cityId, categoryId, shouldAddDate = false) {
    const params = shouldAddDate
      ? { start_date: MomentHelper.getNowAsCheckfrontDate() }
      : {};
    const api = await this.getApi(cityId);
    return await api.items(api.connection[categoryId], params);
  },

  async fetchItemsForMonth(cityId, month, { articleId }) {
    const api = await this.getApi(cityId);
    const params = this.getMonthTimeParams(month);
    return await api.item(articleId, params);
  },

  getMonthTimeParams(month) {
    const {startDate, endDate} = this.getMonthBorders(month);
    return {
      end_date: endDate,
      start_date: startDate,
      times: 1,
    };
  },

  createCalendarItem(month, key, state, timeSlots = []) {
    return {
      month,
      timeSlots,
      state,
      dateKey: key,
      date: MomentHelper.transformCheckfrontToDate(key),
    };
  },

  async fetchCalendarForMonth(cityId, month, { articleId }) {
    const key = `calendar-city-${cityId}-month-${month}-article-${articleId}`;
    const setCallback = async () => {
      const api = await this.getApi(cityId);
      const params = this.getMonthTimeParams(month);
      const result = await api.item(articleId, params);
      if (!result.item) return [];
      const itemId = result.item.item_id;
      const calendarData = [];
      const dates = result.item.rate.dates;
      let dateKeys = Object.keys(dates).filter(key => /\d+/.test(key));

      await Promise.all(dateKeys.map(async (key) => {
        if (dates[key].timeslots.length === 0) {
          calendarData.push(this.createCalendarItem(month, key, 'empty'));
        } else if (MomentHelper.isCheckfrontDateInPast(key)) {
          calendarData.push(this.createCalendarItem(month, key, 'bookedUp'));
        } else if (!MomentHelper.isCheckfrontDateInPast(key)) {
          const dateParams = {
            times: 1,
            date: key
          };
          const result = await api.item(itemId, dateParams);
          const timeSlots = result.item.rate.dates[key].timeslots;
          if (timeSlots.every((slot) => slot.A === 0)) {
            calendarData.push(this.createCalendarItem(month, key, 'bookedUp'));
          } else {
            calendarData.push(this.createCalendarItem(month, key, 'available', timeSlots));
          }
        }
      }));

      return calendarData;
    };
    return strapi.services.redis.getOrSet(key, setCallback, 3 * 60);
  },

  async fetchItemTimes(cityId, date, { articleId, discount_code, number_of_tickets, ticket_parameter }) {
    const api = await this.getApi(cityId);
    const params = {
      times: 1,
      date: MomentHelper.transformDateToCheckfront(date)
    };
    if (discount_code) params.discount_code = discount_code;
    if (number_of_tickets) params[`param[${ticket_parameter}]`] = number_of_tickets;
    return await api.item(articleId, params);
  },

  async fetchItemTickets({ cityId, itemId, date, ticketParameter, tickets }) {
    const api = await this.getApi(cityId);
    const params = {
      times: 1,
      date: MomentHelper.transformDateToCheckfront(date),
    };
    params[`param[${ticketParameter}]`] = tickets;
    return await api.item(itemId, params);
  },

  getMonthBorders(month) {
    const monthMoment = MomentHelper.getMomentForMonth(month);
    const startDate = monthMoment.format(MomentHelper.getCheckfrontDateFormat());
    const endDate = monthMoment.endOf('month').format(MomentHelper.getCheckfrontDateFormat());
    return {startDate, endDate};
  },

  async fetchCalendarItems(cityId, categoryId, month) {
    const api = await this.getApi(cityId);
    const {startDate, endDate} = this.getMonthBorders(month);
    return await api.calendarItems(api.connection[categoryId], startDate, endDate);
  },

  async alterBookingSession(cityId, slip, sessionId, alterOptions) {
    const api = await this.getApi(cityId);
    return api.alterBookingSession(slip, sessionId, alterOptions);
  },

  async getBookingSession(cityId, sessionId) {
    const api = await this.getApi(cityId);
    return await api.getBookingSession(sessionId);
  },

  async deleteBookingSession(cityId, sessionId) {
    const api = await this.getApi(cityId);
    return await api.deleteBookingSession(sessionId);
  },

  async closeBookingSession(cityId, sessionId) {
    const api = await this.getApi(cityId);
    return await api.closeBookingSession(sessionId);
  },

  async createBooking(cityId, sessionId, clientIP, formData) {
    const api = await this.getApi(cityId);
    return await api.createBooking(sessionId, clientIP, formData);
  },
  async getBookingForm(cityId) {
    const api = await this.getApi(cityId);
    return await api.getBookingForm();
  },
  async getBooking(cityId, bookingId) {
    const api = await this.getApi(cityId);
    return await api.getBooking(bookingId);
  },
  async getHost(cityId) {
    const api = await this.getApi(cityId);
    return {
      host: api.getHost()
    };
  },
  async getCheckoutLabels(cityId) {
    const connection = await this.findConnection(cityId);
    if (!connection) throw new Error('This city has no valid checkfront connection');
    return {
      findOutAboutUsLabelId: connection.find_out_about_us_label_id,
      wantsToKnowMoreLabelId: connection.wants_to_know_more_label_id,
      extraInfoLabelId: connection.extra_info_label_id
    };
  }
};
