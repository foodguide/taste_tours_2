'use strict';
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async find(params, populate) {
    const key = 'keyvalues';
    const setCallback = () => {
      return strapi.query('keyvalue').find(params, populate);
    };
    return strapi.services.redis.getOrSet(key, setCallback, 15 * 60);
  },
};

