'use strict';
/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/guides/services.html#core-services)
 * to customize this service
 */

module.exports = {
  async find(params, populate) {
    const key = 'knownbyitems';
    const setCallback = () => {
      return strapi.query('knownbyitem').find(params, populate);
    };
    return strapi.services.redis.getOrSet(key, setCallback, 15 * 60);
  },
};

