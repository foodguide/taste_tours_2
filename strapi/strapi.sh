#!/usr/bin/env bash
set -ea

startProd() {
  echo "starting strapi in production environment"
  npm run build
  npm run start &

  strapiPID=$!
  wait "$strapiPID"
}

startDev() {
  echo "starting strapi in development environment"
  npm run dev &

  strapiPID=$!
  wait "$strapiPID"
}

_stopStrapi() {
  echo "Stopping strapi"
  kill -INT "$strapiPID"
  wait "$strapiPID"
}

trap _stopStrapi TERM INT

cd /usr/src/api

APP_NAME=strapi
DATABASE_CLIENT=${DATABASE_CLIENT:-mongo}
DATABASE_HOST=${DATABASE_HOST:-localhost}
DATABASE_PORT=${DATABASE_PORT:-27017}
DATABASE_NAME=${DATABASE_NAME:-strapi}
DATABASE_SRV=${DATABASE_SRV:-false}
EXTRA_ARGS=${EXTRA_ARGS:-}

cd strapi
rm -rf .cache

NODE_ENV=$1

if [[ $1 == "production" ]]; then
  startProd
fi
if [[ $1 == "prod" ]]; then
  startProd
fi
if [[ $1 == "development" ]]; then
  startDev
fi
if [[ $1 == "dev" ]]; then
  startDev
fi
